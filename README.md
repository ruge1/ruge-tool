# ruge-tool

#### 介绍

java 常用工具类

文档空间地址： https://apidoc.gitee.com/ruge1/ruge-tool

#### 软件架构

软件架构说明

使用Apache Maven部署到OSSRH   https://central.sonatype.org/publish/publish-maven/

基于java8

多种工具类的解决方案，在现有工具类上增加了自己的封装以及使用示例

|  工具类   | 解决方案 | 官网地址|
|  ----  | ----  |----  |
| json  | fastjson2||
| excel  | easyexcel ||
| date  | xk-time ||
| string  | commons-lang3 ||
| 集合  | commons-collections4 ||
| 加解密  | commons-codec ||
| 音视频处理  | javacv ||

#### 安装教程

maven仓库地址

https://search.maven.org/search?q=g:com.gitee.ruge1%20AND%20a:ruge-tool （更新较快）

https://mvnrepository.com/artifact/com.gitee.ruge1/ruge-tool （更新慢一点）

```
<dependency>
  <groupId>com.gitee.ruge1</groupId>
  <artifactId>ruge-tool</artifactId>
  <version>0.0.4</version>
  <exclusions>
    <exclusion>
        <groupId>*</groupId>
        <artifactId>*</artifactId>
    </exclusion>
    </exclusions>
</dependency>
```

#### 使用说明

1 引入依赖 2

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
