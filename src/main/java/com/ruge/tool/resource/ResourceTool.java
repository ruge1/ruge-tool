package com.ruge.tool.resource;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * 加载资源 工具类
 *
 * @author ruge.wu
 * @since 2021-10-21 14:16
 **/
public class ResourceTool {
    /**
     * 根据配置文件的路径，将配置文件加载成字节输入流，存储在内存
     *
     * @param path 配置文件路径  resources下
     * @return 字节输入流
     */
    public static InputStream getResourceAsSteam(String path) {
        return ResourceTool.class.getClassLoader().getResourceAsStream(path);
    }

    /**
     * 根据配置文件的路径，将配置文件加载成字符串
     *
     * @param path 配置文件路径  resources下
     * @return 字符串
     */
    public static String getResourceAsString(String path) {
        InputStream inputStream = ResourceTool.class.getClassLoader().getResourceAsStream(path);
        assert inputStream != null;
        return new BufferedReader(new InputStreamReader(inputStream))
                .lines().parallel().collect(Collectors.joining(System.lineSeparator()));
    }

}
