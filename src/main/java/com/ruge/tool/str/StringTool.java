package com.ruge.tool.str;


import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ruge.wu
 * @version 2020.07.02 14:33
 */
public class StringTool extends StringUtils {
    /**
     * @param str 字符串格式化
     * @param obj 格式化参数
     * @return 格式化后字符
     */
    public static String format(String str, Object... obj) {
        return MessageFormat.format(str, obj);
    }

    /**
     * 创建StringBuilder对象
     *
     * @return StringBuilder对象
     */
    public static StringBuilder builder() {
        return new StringBuilder();
    }

    /**
     * 字符串转Integer
     *
     * @param str 字符串
     * @return 字符串转Integer
     */
    public static Integer strToInteger(Object str) {
        if (isBlank(str)) {
            return null;
        }
        return Integer.parseInt(str.toString());
    }

    /**
     * @param str 目标对象
     * @return 目标对象为空 true 否则 false
     */
    public static boolean isBlank(Object str) {
        if (str instanceof Set) {
            Set set = (Set) str;
            return set.isEmpty();
        }
        if (str instanceof List) {
            List list = (List) str;
            return list.isEmpty();
        }
        if (str instanceof Map) {
            Map map = (Map) str;
            return map.isEmpty();
        }
        return (str == null || "".equals(str) || "".equals(str.toString().trim()));
    }

    /**
     * @param strs 目标对象
     * @return 目标对象为空 true 否则 false
     */
    public static Boolean isBlank(Object... strs) {
        if (null == strs) {
            return true;
        }
        for (Object str : strs) {
            if (isNotBlank(str)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param str 目标对象
     * @return 目标对象不为空 true 否则 false
     */
    public static boolean isNotBlank(Object str) {
        return !isBlank(str);
    }

    /**
     * @param strs 目标对象
     * @return 目标对象不为空 true 否则 false
     */
    public static Boolean isNotBlank(Object... strs) {
        for (Object str : strs) {
            if (isBlank(str)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param str 目标字符串
     * @return 全部都是小写 是true 否false
     */
    public static boolean isAllLowerCase(String str) {
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isUpperCase(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param str 目标字符串
     * @return 全部都是大写 是true 否false
     */
    public static boolean isAllUpperCase(String str) {
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isLowerCase(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 将首字母和带 _ 后第一个字母 转换成大写
     *
     * @param str 带下划线的table字符串
     * @return DTO
     */
    public static String upperTable(String str) {
        // 字符串缓冲区
        StringBuffer sbf = new StringBuffer();
        // 如果字符串包含 下划线
        String keyword1 = "_";
        if (str.contains(keyword1)) {
            // 按下划线来切割字符串为数组
            String[] split = str.split("_");
            // 循环数组操作其中的字符串
            for (int i = 0, index = split.length; i < index; i++) {
                // 递归调用本方法
                String upperTable = upperTable(split[i]);
                // 添加到字符串缓冲区
                sbf.append(upperTable);
            }
        } else {// 字符串不包含下划线
            // 转换成字符数组
            char[] ch = str.toCharArray();
            // 判断首字母是否是字母
            // 关键字符 a
            char keyword2 = 'a';
            // 关键字符 z
            char keyword3 = 'z';
            if (ch[0] >= keyword2 && ch[0] <= keyword3) {
                // 利用ASCII码实现大写
                ch[0] = (char) (ch[0] - 32);
            }
            // 添加进字符串缓存区
            sbf.append(ch);
        }
        // 返回
        return sbf.toString();
    }


    /**
     * 字符串拼接 如果是对象 toString后拼接
     *
     * @param strs 待拼接对象
     * @return 拼接后的字符串
     */
    public static String concat(Object... strs) {
        if (isBlank(strs)) {
            return null;
        }
        return Arrays.stream(strs).reduce(new StringBuilder(), StringBuilder::append, StringBuilder::append).toString();
    }


    /**
     * 当前字符串必须以关键字开始
     *
     * @param str     字符串
     * @param keyWord 关键字
     * @return 构建后的字符串
     */
    public static String mustStartWith(String str, String keyWord) {
        if (isBlank(str)) {
            return keyWord;
        }
        if (str.startsWith(keyWord)) {
            return str;
        } else {
            return keyWord.concat(str).trim();
        }
    }

    /**
     * 当前字符串必须不能以关键字开始
     *
     * @param str     字符串
     * @param keyWord 关键字
     * @return 构建后的字符串
     */
    public static String mustNotStartWith(String str, String keyWord) {
        if (isBlank(str)) {
            return keyWord;
        }
        if (str.startsWith(keyWord)) {
            return str.substring(keyWord.length()).trim();
        } else {
            return str;
        }
    }

    /**
     * 当前字符串必须以关键字结束
     *
     * @param str     字符串
     * @param keyWord 关键字
     * @return 构建后的字符串
     */
    public static String mustEndWith(String str, String keyWord) {
        if (isBlank(str)) {
            return keyWord;
        }
        if (str.endsWith(keyWord)) {
            return str;
        } else {
            return str.concat(keyWord);
        }
    }

    /**
     * 当前字符串必须不能以关键字结束
     *
     * @param str     字符串
     * @param keyWord 关键字
     * @return 构建后的字符串
     */
    public static String mustNotEndWith(String str, String keyWord) {
        if (isBlank(str)) {
            return keyWord;
        }
        if (str.endsWith(keyWord)) {
            return str.substring(0, str.length() - keyWord.length());
        } else {
            return str.trim();
        }
    }

    /**
     * 当前字符串必须不能以关键字结束
     *
     * @param str     字符串
     * @param keyWord 关键字
     * @return 构建后的字符串
     */
    /**
     * java 替换字符串，替换首字符
     *
     * @param str         待替换的字符
     * @param replaceWord 替换的字符
     * @return 替换后的字符
     */
    public static String replaceFirst(String str, String replaceWord) {
        if (isBlank(str)) {
            return replaceWord;
        }
        return replaceWord + str.substring(1);
    }

    /**
     * 获取请求url
     *
     * @param str 字符串
     * @return 请求url
     */
    public static String getRequestUrl(String str) {
        String newStr = str.replace("http://", "").replace("https://", "").trim();
        return newStr.substring(0, newStr.indexOf('/'));
    }

    /**
     * 获取请求url的参数
     *
     * @param str 字符串
     * @return 请求url
     */
    public static String getRequestParams(String str) {
        String newStr = str.replace("http://", "").replace("https://", "").trim();
        return newStr.substring(0, newStr.indexOf("/"));
    }

    public static String upperCaseFirst(String name) {
        char[] cs = name.toCharArray();
        cs[0] -= 32;
        return String.valueOf(cs);
    }

    public static String lowerCaseFirst(String name) {
        //UpperCase大写
        name = name.substring(0, 1).toLowerCase() + name.substring(1);
        return name;
    }

    /**
     * 首字母转小写
     *
     * @param s 字符串
     * @return 首字母转小写
     */
    public static String toLowerCaseFirstOne(String s) {
        if (Character.isLowerCase(s.charAt(0))) {
            return s;
        } else {
            return Character.toLowerCase(s.charAt(0)) + s.substring(1);
        }
    }

    /**
     * 首字母转大写
     *
     * @param s 字符串
     * @return 首字母转大写
     */
    public static String toUpperCaseFirstOne(String s) {
        if (Character.isUpperCase(s.charAt(0))) {
            return s;
        } else {
            return Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }
    }

    /**
     * 获取游戏昵称
     *
     * @param nameLength 名字长度
     * @return 游戏昵称
     */
    public static String getGameNickName(Integer nameLength) {
        return ChineseName.getGameNickName(nameLength);
    }

    /**
     * 判断是否是数字
     *
     * @param s 字符串
     * @return 判断是否是数字
     */
    public static boolean isNumeric(String s) {
        return Character.isDigit(s.charAt(0));
    }
}
