package com.ruge.tool.redisson;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

/**
 * redisson分布式锁 工具类
 *
 * @author ruge.wu
 * @since 2021-10-19 11:37
 **/
public interface DistributedLocker {
    /**
     * lock(), 拿不到lock就不罢休，不然线程就一直block
     *
     * @param lockKey key
     * @return {@link RLock}
     */
    RLock lock(String lockKey);

    /**
     * timeout为加锁时间，单位为秒
     *
     * @param lockKey key
     * @param timeout 超时时间 默认秒
     * @return {@link RLock}
     */
    RLock lock(String lockKey, long timeout);

    /**
     * timeout为加锁时间，时间单位由unit确定
     *
     * @param lockKey key
     * @param unit    时间单位
     * @param timeout 超时时间
     * @return {@link RLock}
     */
    RLock lock(String lockKey, TimeUnit unit, long timeout);

    /**
     * tryLock()，马上返回，拿到lock就返回true，不然返回false。
     * 带时间限制的tryLock()，拿不到lock，就等一段时间，超时返回false.
     *
     * @param lockKey   key
     * @param unit      时间单位
     * @param waitTime  等待时间
     * @param leaseTime 超时时间
     * @return 是否拿到了锁
     */
    boolean tryLock(String lockKey, TimeUnit unit, long waitTime, long leaseTime);

    /**
     * 解锁
     *
     * @param lockKey key
     */
    void unlock(String lockKey);

    /**
     * 解锁
     *
     * @param lock {@link RLock}
     */
    void unlock(RLock lock);
}
