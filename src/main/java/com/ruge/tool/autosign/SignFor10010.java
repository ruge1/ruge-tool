package com.ruge.tool.autosign;

import cn.hutool.http.HttpUtil;
import com.ruge.tool.constants.Constants;
import com.ruge.tool.json.JsonTool;

import java.util.HashMap;
import java.util.Map;

/**
 * SignFor10010 联通签到 
 * @author admin
 * @version 2024/07/26 08:38
 **/
public class SignFor10010 {
    public static final Map<String, String> headers = new HashMap<>();
    public static final String LOGIN_URL = "https://m.client.10010.com/mobileService/login.htm";

    static {
        headers.put("Host", "m.client.10010.com");
        headers.put("Accept", "*/*");
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Connection", "keep-alive");
        headers.put("Cookie", "devicedId=20be54b981ba4188a797f705d77842d6");
        headers.put("User-Agent", "Mozilla/5.0 (Linux; Android 9; MI 6 Build/PKQ1.190118.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/80.0.3987.99 Mobile Safari/537.36; unicom{version:android@8.0200,desmobile:13298899809};devicetype{deviceBrand:Xiaomi,deviceModel:MI 6};{yw_code:}");
        headers.put("Accept-Language", "zh-cn");
        headers.put("Accept-Encoding", "gzip");
        headers.put("Content-Length", "1446");
    }

    public static void sign() {
        Map<String, String> body = new HashMap<>();
//        "deviceOS": "android9",
//                "mobile": self.rsa_encrypt(mobile),
//                "netWay": "Wifi",
//                "deviceCode": "20be54b981ba4188a797f705d77842d6",
//                "isRemberPwd": 'true',
//                "version": "android@" + self.VERSION,
//                "deviceId": "20be54b981ba4188a797f705d77842d6",
//                "password": self.rsa_encrypt(passwd),
//                "keyVersion": 1,
//                "provinceChanel": "general",
//                "appId": self.pid,
//                "deviceModel": "MI 6",
//                "deviceBrand": "Xiaomi",
//                "timestamp": timestamp
        body.put("deviceOS", "android9");
        body.put("mobile", "android9");
        String result = HttpUtil.createPost(LOGIN_URL).addHeaders(headers).body(JsonTool.getObjToJson(body)).execute().body();
    }

    public static Map<String, Object> exec() {
        Map<String, Object> map = new HashMap<>();
        sign();
        return map;
    }
}
