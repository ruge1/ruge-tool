package com.ruge.tool.autosign.api;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruge.tool.autosign.util.HttpURLConnectionUtils;
import com.ruge.tool.constants.Constants;
import com.ruge.tool.date.DateTool;
import com.ruge.tool.json.JsonTool;
import com.ruge.tool.sendMsg.DingDingTool;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 掘进 签到
 *
 * @author ruge.wu
 * @version 2022-01-06 23:04
 **/
@Slf4j
public class JueJinApi {

    // 签到
    public static final String CHECK_IN = "https://api.juejin.cn/growth_api/v1/check_in?aid=2608&uuid=7392842114574763557&spider=0&msToken=6mi9aSkG23Ly58NRvzh_KRo9M1fV3vXwvgI-QxZSfkFnLOoPA2MA0k69tLP0WU_p0KwAzQvGg1wDQr1ul391i-Xtas-rr3XiVfI8RFWBclOojlnkovfS1GA8S_XTJ2Ur&a_bogus=EX-Yfc2KMsm1vwVf/7Dz9CuExWf0YW5igZENeDBv7Uws";
    // 抽奖
    public static final String DRAW = "https://api.juejin.cn/growth_api/v1/lottery/draw?aid=2608&uuid=7392842114574763557&spider=0&msToken=6mi9aSkG23Ly58NRvzh_KRo9M1fV3vXwvgI-QxZSfkFnLOoPA2MA0k69tLP0WU_p0KwAzQvGg1wDQr1ul391i-Xtas-rr3XiVfI8RFWBclOojlnkovfS1GA8S_XTJ2Ur&a_bogus=Q6lD6c2KMsm1vwVf%2FXDz9CuExlj0YWR6gZENeDpT4tw%2F";
    // 获取矿石总数
    public static final String GET_CUR_POINT = "https://api.juejin.cn/growth_api/v1/get_cur_point";
    // 查询今天是否已签到
    public static final String GET_TODAY_STATUS = "https://api.juejin.cn/growth_api/v1/get_today_status";
    // 查询连续签到天数
    public static final String GET_COUNTS = "https://api.juejin.cn/growth_api/v1/get_counts";
    // 查询免费抽奖次数
    public static final String LOTTERY_CONFIG_GET = "https://api.juejin.cn/growth_api/v1/lottery_config/get?aid=2608&uuid=7392842114574763557&spider=0&msToken=-JWCJSLzpDLjdi67w3cSQFnVeC5QVSkboSyqovOtxOjjvVkueiaH6D4xDagTivoigBabGKaj_GmiLe4yFKs3Tsu5TRzPQoVZW-hBtM6TgWJYGVVZsRlQ9DA0UaK1FXU%3D&a_bogus=xj-Evc2tMsm17DVf67Dz9twmxWE0YW-CgZENa9PUNzqX";
    // 沸点list接口
    public static final String RECOMMEND = "https://api.juejin.cn/recommend_api/v1/short_msg/recommend";
    // 沸点点赞接口
    public static final String SAVE = "https://api.juejin.cn/interact_api/v1/digg/save";
    // 发布沸点接口
    public static final String PUBLISH = "https://api.juejin.cn/content_api/v1/short_msg/publish";
    // 沸点评论接口
    public static final String COMMENT_PUBLISH = "https://api.juejin.cn/interact_api/v1/comment/publish?aid=2608&uuid=7392842114574763557&spider=0&msToken=6mi9aSkG23Ly58NRvzh_KRo9M1fV3vXwvgI-QxZSfkFnLOoPA2MA0k69tLP0WU_p0KwAzQvGg1wDQr1ul391i-Xtas-rr3XiVfI8RFWBclOojlnkovfS1GA8S_XTJ2Ur&a_bogus=EylQhO2KMsm1vqOfM7Dz9CuXx-80YW59gZENeD5oJzwI";
    // 文章列表
    public static final String ARTICLE_RANK = "https://api.juejin.cn/content_api/v1/content/article_rank?category_id=1&type=hot&count=50&from=0";
    // 文章点赞
    public static final String ARTICLE_SAVE = "https://api.juejin.cn/interact_api/v1/digg/save";
    // 文章收藏
    public static final String ARTICLE_ADD = "https://api.juejin.cn/interact_api/v2/collectionset/add_article?aid=2608&uuid=7392842114574763557&spider=0";
    // 评论文章
    public static final String ARTICLE_COMMENT = "https://api.juejin.cn/interact_api/v1/comment/publish";
    // 查询今日获得掘友分
    public static final String TASK_LIST = "https://api.juejin.cn/growth_api/v1/user_growth/task_list";
    // 掘友列表
    public static final String RANK = "https://api.juejin.cn/user_api/v1/quality_user/rank";
    // 关注掘友
    public static final String DO = "https://api.juejin.cn/interact_api/v1/follow/do";
    // 取关掘友
    public static final String UNDO = "https://api.juejin.cn/interact_api/v1/follow/undo";
    // 查询总掘友分
    public static final String PROGRESS = "https://api.juejin.cn/growth_api/v1/user_growth/progress";

    public static final Map<String, String> headers = new HashMap<>();

    static {
        headers.put("Cookie", Constants.JUEJIN_COOKIE);
        headers.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0");
//        headers.put("accept", "*/*");
//        headers.put("accept-language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
//        headers.put("Accept-Encoding", "gzip, deflate, br");
//        headers.put("content-type", "application/json");
//        headers.put("Connection", "keep-alive");
//        headers.put("sec-fetch-mode", "navigate");
//        headers.put("sec-fetch-site", "none");
//        headers.put("priority", "u=0, i");
//        headers.put("sec-fetch-user", "?1");
//        headers.put("pragma", "no-cache");
//        headers.put("sec-ch-ua", "Not)A;Brand;v=99, Microsoft Edge;v=127, Chromium;v=127");
//        headers.put("sec-ch-ua-mobile", "?0");
//        headers.put("sec-ch-ua-platform", "Windows");
//        headers.put("upgrade-insecure-requests", "1");
//        headers.put("cache-control", "no-cache");
//        headers.put("sec-fetch-dest", "document");
//        headers.put("origin", "https://juejin.cn");
//        headers.put("referer", "https://juejin.cn/");
    }

    // 签到
    public static JSONObject checkIn(String cookie) throws Exception {
        String response = HttpURLConnectionUtils.post(CHECK_IN, null, headers, null);
        return JSONObject.parseObject(response);
    }

    // 抽奖
    public static JSONObject draw(String cookie) throws Exception {
        String response = HttpURLConnectionUtils.post(DRAW, null, headers, null);
        return JSONObject.parseObject(response);
    }

    // 获取矿石总数
    public static JSONObject getCountOfOre(String cookie) throws Exception {
        String response = HttpURLConnectionUtils.get(GET_CUR_POINT, null, headers);
        return JSONObject.parseObject(response);
    }

    // 查询签到状态
    public static JSONObject getStateOfCheckIn(String cookie) throws Exception {
        String response = HttpURLConnectionUtils.get(GET_TODAY_STATUS, null, headers);
        return JSONObject.parseObject(response);
    }

    // 查询连续签到天数和总签到天数
    public static JSONObject getDayCountOfCheckIn(String cookie) throws Exception {
        String response = HttpURLConnectionUtils.get(GET_COUNTS, null, headers);
        return JSONObject.parseObject(response);
    }

    // 查询免费抽奖次数
    public static JSONObject getCountOfFreeDraw(String cookie) throws Exception {
        String body = HttpUtil.createGet(LOTTERY_CONFIG_GET).addHeaders(headers).execute().body();
        String response = HttpURLConnectionUtils.get(LOTTERY_CONFIG_GET, null, headers);
        return JSONObject.parseObject(response);
    }

    // 查询沸点列表 {"id_type":4,"sort_type":300,"cursor":"0","limit":5}
    public static JSONObject getBoilingPointList(JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(RECOMMEND, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 点赞沸点
    public static JSONObject upvoteBoilingPoint(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(SAVE, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 发布沸点
    public static JSONObject publishBoilingPoint(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(PUBLISH, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 评论沸点 请求体示例 : {"client_type":2608,"item_id":"7399985328733700146","item_type":4,"comment_content":"大佬666哇[赞][赞][赞]","comment_pics":[]}
    public static JSONObject commentBoilingPoint(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(COMMENT_PUBLISH, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 查询文章列表
    public static JSONObject getArticleList(String cookie) throws Exception {
        String response = HttpURLConnectionUtils.get(ARTICLE_RANK, null, headers);
        return JSONObject.parseObject(response);
    }

    // 点赞文章 请求体示例 : {"item_id":"7293356159939313703","item_type":2,"client_type":2608}
    public static JSONObject upvoteArticle(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(ARTICLE_SAVE, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 文章收藏
    public static JSONObject addArticle(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(ARTICLE_ADD, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 评论文章 请求体示例 : {"client_type":2608,"item_id":"7293356159939313703","item_type":2,"comment_content":"666","comment_pics":[]}
    public static JSONObject articleComment(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(ARTICLE_COMMENT, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 查询掘友分 请求体示例 : {"growth_type":1}
    public static JSONObject taskList(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(TASK_LIST, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 掘友列表 请求体示例 : {"item_rank_type":1,"item_sub_rank_type":"6809637769959178254"}
    public static JSONObject rank(JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(RANK, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 关注掘友 请求体示例 : {"id":"1732486057428952","type":1}
    public static JSONObject subscribe(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(DO, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 取关掘友 请求体示例 : {"id":"1732486057428952","type":1}
    public static JSONObject unSubscribe(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(UNDO, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }

    // 查询总掘友分 请求体示例 : {"growth_type":1}
    public static JSONObject progress(String cookie, JSONObject bodyParam) throws Exception {
        String response = HttpURLConnectionUtils.post(PROGRESS, null, headers, bodyParam);
        return JSONObject.parseObject(response);
    }


}
