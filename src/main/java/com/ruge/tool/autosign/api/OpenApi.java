package com.ruge.tool.autosign.api;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ruge.tool.json.JsonTool;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * OpenApi
 * https://api.oioweb.cn/
 * @author admin
 * @version 2024/08/05 15:48
 **/
public class OpenApi {
    /**
     * 每日60秒读懂世界新闻
     */
    public static String Get60sWorldInsight() {
        String body = HttpUtil.createGet("https://api.oioweb.cn/api/common/Get60sWorldInsight").execute().body();
        List<String> str = (List<String>) JsonTool.getJsonToMap(body).get("result");
        return str.stream().map(i -> i.toString()).collect(Collectors.joining("\n\n"));
    }
}
