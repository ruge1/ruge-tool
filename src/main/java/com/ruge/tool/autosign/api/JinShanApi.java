package com.ruge.tool.autosign.api;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSONObject;

/**
 * JinShanApi  
 * @author admin
 * @version 2024/08/05 09:45
 **/
public class JinShanApi {
    public static final String DAILY_SENTENCE = "https://open.iciba.com/dsapi/";

    // 金山每日一句
    public static JSONObject dailySentence() {
        String result = HttpUtil.createPost(DAILY_SENTENCE).execute().body();
        return JSONObject.parseObject(result);
    }
}
