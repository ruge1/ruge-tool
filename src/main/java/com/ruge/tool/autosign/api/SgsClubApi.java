package com.ruge.tool.autosign.api;

import cn.hutool.http.HttpUtil;
import com.ruge.tool.autosign.util.HttpURLConnectionUtils;
import com.ruge.tool.constants.Constants;
import com.ruge.tool.date.DateTool;
import com.ruge.tool.sendMsg.DingDingTool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * 三国杀社区
 *
 * @author admin
 * @version 2024/08/05 14:08
 **/
public class SgsClubApi {
    public static final Map<String, String> headers = new HashMap<>();

    public static final String SIGN = "https://club.sanguosha.com/plugin.php?id=dsu_paulsign:sign&operation=qiandao&infloat=1&inajax=1";


//      const header= {
//        Host: club.sanguosha.com,
//                Sec-Fetch-Site: same-origin,
//                Accept-Encoding: gzip, deflate, br,
//                Cookie: IwdU_2132_lastact=1713015996%09plugin.php%09; IwdU_2132_mobile=no; IwdU_2132_noticeTitle=1; IwdU_2132_sendmail=1; IwdU_2132_atarget=1; IwdU_2132_forum_lastvisit=D_61_1713015856; IwdU_2132_st_t=289159%7C1713015856%7C5796c1595b853aa2fb63b9228f30a939; IwdU_2132_auth=0107w0ZY%2B%2Fl8IHM7qs5PqiWU5FOZ0dqvEaLSIM2%2BqYyjSbnZ1b4g%2B1itSRuIF94KEK%2F3oKYfV21rF0etaX17Ue3NG7g; IwdU_2132_lastcheckfeed=289159%7C1713015853; IwdU_2132_nofavfid=1; IwdU_2132_ulastactivity=1713015853%7C0; IwdU_2132_lastvisit=1713012246; IwdU_2132_saltkey=dUS80AA8; acw_tc=0b32807617130158468018374e3e0c9607e954935bbdeca12f4175ef3ec282; SGS_DEVICEID=WEB-12255F68-CEC7-4000-9DFF-F4E756C74A66; SGS_DEVICEID_SPARE=WEB-12255F68-CEC7-4000-9DFF-F4E756C74A66; Hm_lvt_741c1946c0ae40c84a3db0b843e0b944=1712107450,1712109092,1712126920,1712882997; Hm_lvt_fb3e47332f1c41f3e8dd1961d001377f=1712107450,1712109092,1712126920,1712882997,
//                Connection: keep-alive,
//                Sec-Fetch-Mode: navigate,
//                Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8,
//                User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 16_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.6 Mobile/15E148 Safari/604.1,
//                Referer: https://club.sanguosha.com/plugin.php?id=dsu_paulsign:sign,
//                Sec-Fetch-Dest: document,
//                Accept-Language: zh-CN,zh-Hans;q=0.9
//    }


    static {
        headers.put("Cookie", Constants.SGS_CLUB_COOKIE);
        headers.put("sec-ch-ua", "Not_A Brand;v=8 Chromium;v=120 Google Chrome;v=120");
        headers.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        headers.put("content-type", "application/x-www-form-urlencoded");
        headers.put("sec-ch-ua-mobile", "?0");
        headers.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0");
        headers.put("sec-ch-ua-platform", "Windows");
        headers.put("Sec-Fetch-Site", "same-origin");
        headers.put("sec-fetch-mode", "cors");
        headers.put("Sec-Fetch-Dest", "document");
        headers.put("Accept-Encoding", "gzip, deflate, br");
        headers.put("Accept-Language", "zh-CNzh;q=0.9");
        headers.put("Connection", "keep-alive");
        headers.put("Cache-Control", "no-cache");
        headers.put("Host", "club.sanguosha.com");
        headers.put("origin", "https://club.sanguosha.com");
        headers.put("Referer", "https://club.sanguosha.com/dsu_paulsign-sign.html");
        headers.put("Content-Length", "10");
    }

    public static void sign() {
//        StringBuilder builder = new StringBuilder();
//        String command = "curl 'https://club.sanguosha.com/plugin.php?id=dsu_paulsign:sign&operation=qiandao&infloat=1&inajax=1' \\\n" +
//                "  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7' \\\n" +
//                "  -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6' \\\n" +
//                "  -H 'cache-control: no-cache' \\\n" +
//                "  -H 'content-type: application/x-www-form-urlencoded' \\\n" +
//                "  -H 'cookie: SGS_DEVICEID=WEB-E6A74699-4232-4DD4-8298-F70F11BBC230; SGS_DEVICEID_SPARE=WEB-E6A74699-4232-4DD4-8298-F70F11BBC230; Hm_lvt_fb3e47332f1c41f3e8dd1961d001377f=1721435621; Hm_lvt_741c1946c0ae40c84a3db0b843e0b944=1721435621; IwdU_2132_saltkey=DjaJooZf; IwdU_2132_lastvisit=1722834394; IwdU_2132_nofavfid=1; acw_tc=0b32825617235094418335689e2ff1c79dd42beb963f06c992f93705aa9b43; IwdU_2132_sendmail=1; Hm_lvt_6ab30bf40886fd9f92630d1295b2b7eb=1722837996,1723509443; HMACCOUNT=2DFB110BE61C7B28; Hm_lvt_39b895e77c719d1c57281c105226d489=1722837996,1723509443; IwdU_2132_ulastactivity=1723509454%7C0; IwdU_2132_auth=51b9%2FMoFQFqypL0nHzcssFWKCQT8Hpxn%2F0ee8TrG0FNBcc%2B%2F9sG0ewg%2FPHmMn0x2giMbH%2FWAxYMPeBm6INflDKe6FPE; IwdU_2132_lastcheckfeed=108325%7C1723509454; IwdU_2132_checkfollow=1; IwdU_2132_checkpm=1; IwdU_2132_noticeTitle=1; IwdU_2132_lastact=1723509465%09plugin.php%09; Hm_lpvt_39b895e77c719d1c57281c105226d489=1723509466; Hm_lpvt_6ab30bf40886fd9f92630d1295b2b7eb=1723509466' \\\n" +
//                "  -H 'origin: https://club.sanguosha.com' \\\n" +
//                "  -H 'pragma: no-cache' \\\n" +
//                "  -H 'priority: u=0, i' \\\n" +
//                "  -H 'referer: https://club.sanguosha.com/plugin.php?id=dsu_paulsign:sign' \\\n" +
//                "  -H 'sec-ch-ua: \"Not)A;Brand\";v=\"99\", \"Microsoft Edge\";v=\"127\", \"Chromium\";v=\"127\"' \\\n" +
//                "  -H 'sec-ch-ua-mobile: ?0' \\\n" +
//                "  -H 'sec-ch-ua-platform: \"Windows\"' \\\n" +
//                "  -H 'sec-fetch-dest: iframe' \\\n" +
//                "  -H 'sec-fetch-mode: navigate' \\\n" +
//                "  -H 'sec-fetch-site: same-origin' \\\n" +
//                "  -H 'sec-fetch-user: ?1' \\\n" +
//                "  -H 'upgrade-insecure-requests: 1' \\\n" +
//                "  -H 'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0' \\\n" +
//                "  --data-raw 'formhash=569fc4c7'";
//        try {
//            Process process = Runtime.getRuntime().exec(command);
//            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//            String line;
//            while ((line = reader.readLine()) != null) {
//                builder.append(line);
//                System.out.println(builder);
//                builder.append(System.lineSeparator());
//                System.out.println(builder);
//            }
//        } catch (Exception e) {
//           e.printStackTrace();
//        }
        String body = HttpUtil.createPost(SIGN).addHeaders(headers).execute().body();
        DingDingTool.sendMsg("三国杀社区成长助手\n通知时间：" + DateTool.getLocalDateTime() + "\n通知内容：" + body);
    }
}
