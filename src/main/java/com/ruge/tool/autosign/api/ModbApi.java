package com.ruge.tool.autosign.api;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ruge.tool.constants.Constants;
import com.ruge.tool.json.JsonTool;
import com.ruge.tool.sendMsg.DingDingTool;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * SignModb 摩天轮签到 
 * @author admin
 * @version 2024/08/02 15:45
 **/
@Slf4j
public class ModbApi {
    public static final Map<String, String> headers = new HashMap<>();

    static {
        headers.put("cookie", Constants.MODB_COOKIE);
        headers.put("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2MjY4NjAiLCJleHAiOjE3MjMxODk0MDAsImJpelR5cGUiOiJtb2RiIiwicm9sZU5hbWUiOiJST0xFX3VzZXIiLCJwZXJtaXNzaW9ucyI6W119.42sAWGv6ggY-Y2yBkh0AetpltwAKUB8QYzmeTze3WlmQsmHYbDwLWTq0eeeHDr3YIcWtIqcKvtZlDPPoY9FPOQ");
        headers.put("sec-ch-ua", "Not_A Brand;v=8 Chromium;v=120 Google Chrome;v=120");
        headers.put("accept", "application/json,text/plain,*/*");
        headers.put("content-type", "application/json");
        headers.put("sec-ch-ua-mobile", "?0");
        headers.put("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/120.0.0.0 Safari/537.36");
        headers.put("sec-ch-ua-platform", "Windows");
        headers.put("sec-fetch-site", "same-site");
        headers.put("sec-fetch-mode", "cors");
        headers.put("sec-fetch-dest", "empty");
        headers.put("accept-encoding", "gzip deflate br");
        headers.put("accept-language", "zh-CNzh;q=0.9");
        headers.put("User-Agent", "PostmanRuntime-ApipostRuntime/1.1.0");
        headers.put("Connection", "keep-alive");
        headers.put("Cache-Control", "no-cache");
        headers.put("Host", "www.modb.pro");
        headers.put("origin", "https://www.modb.pro");
        headers.put("referer", "https://www.modb.pro/point/signin");
        headers.put("Content-Length", "10");
    }


    // 签到
    public static JSONObject sign() {
        String url = "https://www.modb.pro/api/user/checkIn";
        String result = HttpUtil.createPost(url).addHeaders(headers).execute().body();
        log.info("摩天轮_签到 url:{},response:{}", url, result);
        return JSONObject.parseObject(result);
    }

    // 发布数说
    public static JSONObject addDatalks() {
        String url = "https://www.modb.pro/api/datalks/save";
        // 调用金山每日一句接口
        JSONObject dailySentenceJsonObject = JinShanApi.dailySentence();
        String result = HttpUtil.createPost(url).addHeaders(headers).body("{\"content\":\""+dailySentenceJsonObject.get("content").toString().replace(String.valueOf('\"'), "'")+"\",\"imageUrls\":[],\"polls\":null,\"videoUrl\":\"\",\"topicId\":\"\"}").execute().body();
        log.info("摩天轮_签到 url:{},response:{}", url, result);
        return JSONObject.parseObject(result);
    }

    public static void exec() {
        JSONObject sign = sign();
        addDatalks();
        DingDingTool.sendMsg("摩天轮签到" + JsonTool.getObjToJson(sign));
    }
}
