package com.ruge.tool.autosign;

import cn.hutool.core.date.DateTime;
import com.ruge.tool.autosign.api.ModbApi;
import com.ruge.tool.autosign.api.SgsClubApi;
import com.ruge.tool.autosign.job.JueJinJob;
import com.ruge.tool.constants.Constants;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动签到 工具类
 *
 * @author ruge.wu
 * @version 2022-01-06 23:04
 **/
@Slf4j
public class AutoSignTool {

    public static void autoSign() {
        log.info("开始签到 at" + DateTime.now());
        SignBaiDuTieBa.exec();
        ModbApi.exec();
        SgsClubApi.sign();
        JueJinJob.exec(Constants.JUEJIN_COOKIE);
        log.info("结束签到 at" + DateTime.now());
    }
}
