package com.ruge.tool.autosign;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ruge.tool.constants.Constants;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * SignQuark 夸克签到 
 * @author admin
 * @version 2024/08/02 08:45
 **/
@Slf4j
public class SignQuark {
    public static final Map<String, String> headers = new HashMap<>();

    static {
        headers.put("cookie", Constants.QUARK_COOKIE);
        headers.put("sec-ch-ua", "Not_A Brand;v=8 Chromium;v=120 Google Chrome;v=120");
        headers.put("accept", "application/json,text/plain,*/*");
        headers.put("content-type", "application/json");
        headers.put("sec-ch-ua-mobile", "?0");
        headers.put("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/120.0.0.0 Safari/537.36");
        headers.put("sec-ch-ua-platform", "Windows");
        headers.put("origin", "https://pan.quark.cn");
        headers.put("sec-fetch-site", "same-site");
        headers.put("sec-fetch-mode", "cors");
        headers.put("sec-fetch-dest", "empty");
        headers.put("referer", "https://pan.quark.cn/");
        headers.put("accept-encoding", "gzip deflate br");
        headers.put("accept-language", "zh-CNzh;q=0.9");
        headers.put("User-Agent", "PostmanRuntime-ApipostRuntime/1.1.0");
        headers.put("Connection", "keep-alive");
        headers.put("Cache-Control", "no-cache");
        headers.put("Host", "drive-m.quark.cn");
        headers.put("Content-Length", "10");
    }

    // 查询签到状态
    public static JSONObject getSignStatus() {
        String url = "https://drive-m.quark.cn/1/clouddrive/capacity/growth/info?pr=ucpro&fr=pc&uc_param_str=";
        String result = HttpUtil.createGet(url).addHeaders(headers).execute().body();
        log.info("夸克_查询签到状态 url:{},response:{}", url, result);
        return JSONObject.parseObject(result);
    }

    // 签到
    public static JSONObject sign() {
        String url = "https://drive-m.quark.cn/1/clouddrive/capacity/growth/sign?pr=ucpro&fr=pc&uc_param_str=";
        String result = HttpUtil.createPost(url).addHeaders(headers).execute().body();
        log.info("夸克_签到 url:{},response:{}", url, result);
        return JSONObject.parseObject(result);
    }

    public static Map<String, Object> exec() {
        Map<String, Object> map = new HashMap<>();
        sign();
        map.put(" 签到:", getSignStatus());
        return map;
    }
}
