package com.ruge.tool.baidu;


import com.ruge.tool.http.HttpExplorer;
import com.ruge.tool.http.HttpResponse;
import com.ruge.tool.json.JsonTool;
import com.ruge.tool.map.MapTool;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * description
 * create Time at 2022/7/28 14:12
 *
 * @author alice.ruge
 * @since 0.0.7
 */
public class BaiDuApi {

    /**
     * 获取API访问token
     * 该token有一定的有效期，需要自行管理，当失效时需重新获取.
     *
     * @param ak - 百度云官网获取的 API Key
     * @param sk - 百度云官网获取的 Secret Key
     * @return assess_token 示例：
     * "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567"
     */
    public static String getAuth(String ak, String sk) {
        // 获取token地址
        String authHost = "https://aip.baidubce.com/oauth/2.0/token?";
        String getAccessTokenUrl = authHost
                // 1. grant_type为固定参数
                + "grant_type=client_credentials"
                // 2. 官网获取的 API Key
                + "&client_id=" + ak
                // 3. 官网获取的 Secret Key
                + "&client_secret=" + sk;
        try {
            URL realUrl = new URL(getAccessTokenUrl);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.err.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = "";
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            /**
             * 返回结果示例
             */
            System.err.println("result:" + result);
            String access_token = JsonTool.getJsonToStringMap(result).get("access_token");
            return access_token;
        } catch (Exception e) {
            System.err.printf("获取token失败！");
            e.printStackTrace(System.err);
        }
        return null;
    }

    /**
     * 音频文件转写
     */
    public static String AudioFileTransfer(String token, String url) {
        Map<String, String> body = MapTool.newHashMap("");
        body.put("speech_url", url);
        body.put("format", "m4a");
        body.put("pid", "80001");
        body.put("rate", "16000");
        HttpResponse response = HttpExplorer.builder().build().post("https://aip.baidubce.com/rpc/2.0/aasr/v1/create?access_token=" + token, body);
        return response.getData().toString();
    }
}
