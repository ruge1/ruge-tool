package com.ruge.tool.enums;

import com.ruge.tool.base.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 排序枚举类
 *
 * @author ruge.wu
 * @since 2021-10-08 14:41
 **/
@Getter
@AllArgsConstructor
public enum OrderEnum implements BaseEnum {
    /**
     * 排序枚举类
     */
    DESC("DESC", "降序"),
    ASC("ASC", "升序");
    private String code;
    private String desc;

    @Override
    public String toString() {
        return code + ":" + desc;
    }
}