package com.ruge.tool.enums;

import com.ruge.tool.base.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 排序枚举类
 *
 * @author ruge.wu
 * @since 2021-10-08 14:41
 **/
@Getter
@AllArgsConstructor
public enum DateEnum implements BaseEnum {
    /**
     * 年月日枚举类
     */
    YEAR("YEAR", "年"),
    MONTH("MONTH", "月"),
    DAY("DAY", "日"),
    HOUR("HOUR", "24小时制"),
    MINUTE("MINUTE", "分钟"),
    SECOND("SECOND", "秒钟"),
    ;
    private String code;
    private String desc;

    @Override
    public String toString() {
        return code + ":" + desc;
    }
}