package com.ruge.tool.enums;

import com.ruge.tool.base.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 格式化枚举类
 *
 * @author ruge.wu
 * @since 2021-10-11 14:18
 **/
@Getter
@AllArgsConstructor
public enum DateFormatEnum implements BaseEnum {
    /**
     * 格式化枚举类
     */
    DATE_SHORT("DATE_SHORT", "yyyyMMdd"),
    DATE_MEDIUM("DATE_MEDIUM", "yyyy-MM-dd"),
    DATE_FULL("DATE_FULL", "yyyy年MM月dd日 E"),
    DATE_LONG("DATE_LONG", "yyyy年MM月dd日"),

    TIME_SHORT("TIME_SHORT", "HHmmss"),
    TIME_MEDIUM("TIME_MEDIUM", "HH:mm:ss"),
    TIME_FULL("TIME_FULL", "a hh时mm分ss秒"),
    TIME_LONG("TIME_LONG", "HH时mm分ss秒"),

    DATE_TIME_SHORT("DATE_TIME_SHORT", "yyyyMMddhhmmss"),
    DATE_TIME_MEDIUM("DATE_TIME_MEDIUM", "yyyy-MM-dd HH:mm:ss"),
    DATE_TIME_FULL("DATE_TIME_FULL", "yyyy年MM月dd日 E a hh时mm分ss秒"),
    DATE_TIME_LONG("DATE_TIME_LONG", "yyyy年MM月dd日 HH时mm分ss秒"),
    ;
    private String code;
    private String desc;

    @Override
    public String toString() {
        return code + ":" + desc;
    }


}