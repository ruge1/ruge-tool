package com.ruge.tool.coll;


import cn.hutool.core.util.ObjectUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.formula.functions.T;

import java.util.*;

/**
 * CollTool 工具类
 *
 * @author ruge.wu
 * @since 2021/6/3 20:20
 **/
public class CollTool {
    /**
     * 集合转换成list
     *
     * @param collection 集合
     * @param <T>        集合的泛型
     * @return list集合
     */
    public static <T> List<T> toList(Collection<T> collection) {
        List<T> list = new ArrayList<>();
        CollectionUtils.addAll(list, collection);
        return list;
    }

    /**
     * 数组转list
     *
     * @param elements 元素
     * @param <T>      泛型
     * @return list集合
     */
    @SafeVarargs
    public static <T> List<T> toList(T... elements) {
        List<T> list = new ArrayList<>();
        CollectionUtils.addAll(list, elements);
        return list;
    }

    /**
     * 集合转换成set
     *
     * @param collection 集合
     * @param <T>        泛型
     * @return set集合
     */
    public static <T> Set<T> toSet(Collection<T> collection) {
        Set<T> set = new HashSet<>();
        CollectionUtils.addAll(set, collection);
        return set;
    }

    /**
     * 集合转换成set
     *
     * @param elements 元素
     * @param <T>      泛型
     * @return set集合
     */
    @SafeVarargs
    public static <T> Set<T> toSet(T... elements) {
        Set<T> set = new HashSet<>();
        CollectionUtils.addAll(set, elements);
        return set;
    }

    /**
     * 集合倒序
     *
     * @param <T>  泛型
     * @param list list集合
     */
    public static <T> void reverse(List<T> list) {
        Collections.reverse(list);
    }

    /**
     * list结婚随机排序
     *
     * @param list {@link List}
     */
    public static void shuffle(List<?> list) {
        shuffle(list, new Random());
    }

    /**
     * list结婚随机排序
     *
     * @param rnd  {@link Random}
     * @param list {@link List}
     */
    public static void shuffle(List<?> list, Random rnd) {
        Collections.shuffle(list, rnd);
    }

    /**
     * 满足条件 list 才添加数据
     */
    public static void conditionAdd(Boolean condition, Collection<Object> list, Object obj) {
        if (condition) {
            list.add(obj);
        }
    }

    /**
     * condition为空才会添加元素
     */
    public static void conditionBlackAdd(Object condition, Collection<Object> list, Object obj) {
        if (ObjectUtil.isEmpty(condition)) {
            list.add(obj);
        }
    }

    /**
     * condition非空才会添加元素
     */
    public static void conditionNotBlackAdd(Object condition, Collection<Object> list, Object obj) {
        if (!ObjectUtil.isEmpty(condition)) {
            list.add(obj);
        }
    }


}
