package com.ruge.tool.state;

import lombok.Data;

/**
 * description: Content
 * create time at 2022/12/12 17:04
 *
 * @author alice.ruge
 * @since 0.0.3
 */

@Data
public class Content {

    private State state;
}
