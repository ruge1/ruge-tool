package com.ruge.tool.state;

/**
 * description:
 * create time at 2022/12/12 17:19
 *
 * @author alice.ruge
 * @since 0.0.3
 */
public class StopState implements State {
    @Override
    public void doAction(Content content) {
        System.out.println("键入关闭按钮");
        content.setState(this);
    }

    @Override
    public String toString() {
        return "关闭状态";
    }
}
