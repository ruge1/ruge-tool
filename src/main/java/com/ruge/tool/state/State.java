package com.ruge.tool.state;

/**
 * description: 状态接口
 * create time at 2022/12/12 17:03
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public interface State {
    public void doAction(Content content);
}
