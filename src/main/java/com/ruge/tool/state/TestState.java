package com.ruge.tool.state;

/**
 * description:
 * create time at 2022/12/12 17:20
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public class TestState {
    public static void main(String[] args) {
        Content content = new Content();

        State startState = new StartState();
        startState.doAction(content);

        System.out.println(content.getState().toString());

        State stopState = new StopState();
        stopState.doAction(content);

        System.out.println(content.getState().toString());

    }
}
