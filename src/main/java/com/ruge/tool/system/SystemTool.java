package com.ruge.tool.system;

/**
 * description: 操作系统工具类
 * create time at 2022/6/6 10:12
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public class SystemTool {

    /**
     * 获取当前系统的换行符
     *
     * @return 换行符
     */
    public static String lineSeparator() {
        return System.getProperty("line.separator");
    }
}