package com.ruge.tool.ex;

import java.util.function.Consumer;

/**
 * description: 空值与非空值分支处理
 * create time at 2022/11/27 19:26
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public interface PresentOrElseHandler<T extends Object> {

    /**
     * 值不为空时执行消费操作
     * 值为空时执行其他的操作
     *
     * @param action      值不为空时，执行的消费操作
     * @param emptyAction 值为空时，执行的操作
     **/
    void presentOrElseHandle(Consumer<? super T> action, Runnable emptyAction);

}