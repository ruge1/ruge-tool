package com.ruge.tool.ex;

/**
 * description: BranchHandle
 * create time at 2022/11/27 19:23
 *
 * @author alice.ruge
 * @since 0.0.3
 */

@FunctionalInterface
public interface BranchHandle {

    /**
     * 分支操作
     *
     * @param trueHandle  为true时要进行的操作
     * @param falseHandle 为false时要进行的操作
     **/
    void trueOrFalseHandle(Runnable trueHandle, Runnable falseHandle);

}
