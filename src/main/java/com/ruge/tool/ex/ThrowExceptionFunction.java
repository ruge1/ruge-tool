package com.ruge.tool.ex;

/**
 * description: 抛异常接口
 * create time at 2022/11/27 19:19
 *
 * @author alice.ruge
 * @since 0.0.3
 */

@FunctionalInterface
public interface ThrowExceptionFunction {

    /**
     * 抛出异常信息
     *
     * @param message 异常信息
     * @return void
     **/
    void throwMessage(String message);
}
