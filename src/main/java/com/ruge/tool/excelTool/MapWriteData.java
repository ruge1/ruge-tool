package com.ruge.tool.excelTool;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * description excel工具类
 * create Time at 2022/6/21 15:39
 *
 * @author alice.ruge
 * @since 0.0.7
 */
@Data
public class MapWriteData {
    /**
     * 文件名
     */
    private String fileName;
    /**
     * sheet名
     */
    private String sheetName;
    /**
     * 表头数组
     * String[] headMap = { "项目名称", "楼栋名称", "单元名称", "楼层名称", "房间名称", "业主/租户姓名", "房间状态", "房间功能","认证人数" };
     */
    private String[] headMap;
    /**
     * 对应数据字段数组
     * String[] dataStrMap={"hName","bName","uName","fName","pName","cName","pState","pFunction","pNum"};
     */
    private String[] dataStrMap;
    /**
     * 数据集合
     */
    private List<Map<String, Object>> dataList;
}
