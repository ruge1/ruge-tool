package com.ruge.tool.base;

/**
 * 基础枚举类
 *
 * @author ruge.wu
 * @since 2021-10-09 22:45
 **/
public interface BaseEnum {

    /**
     * 基础枚举类  获取code
     *
     * @return 枚举code
     */
    String getCode();
}
