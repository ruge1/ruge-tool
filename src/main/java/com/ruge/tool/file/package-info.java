/**
 * 操作文件的工具类,它们基于File对象工作，包括读，写，拷贝和比较文件
 * commons-io:commons-io
 */
package com.ruge.tool.file;