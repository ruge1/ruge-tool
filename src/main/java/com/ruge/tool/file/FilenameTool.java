package com.ruge.tool.file;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

/**
 * description
 * create Time at 2022/6/21 13:39
 *
 * @author alice.ruge
 * @since 0.0.7
 */
public class FilenameTool extends FilenameUtils {
    /**
     * 系统分隔符
     */
    public static final char SYSTEM_SEPARATOR = File.separatorChar;
}
