package com.ruge.tool.list;

import java.util.List;
import java.util.stream.Collectors;

/**
 * description 树形结构工具类
 * create Time at 2022/6/13 15:48
 *
 * @author alice.ruge
 * @since 0.0.7
 */
public class TreeTool {
    /**
     * 树形结构转model
     *
     * @param list 全部节点列表
     * @param pid  根节点ID
     * @param <T>  节点数据model
     * @return 树结构数据
     */
    public static <T extends TreeNodeDTO> List<T> listToTree(List<T> list, Long pid) {
        return list.stream()
                .filter(treeNodeDTO -> treeNodeDTO.getParentId().equals(pid))
                .peek(treeNodeDTO -> treeNodeDTO.setChildrenList(listToTree(list, treeNodeDTO.getId())))
                .collect(Collectors.toList());
    }
}
