package com.ruge.tool.list;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * description 树节点对象
 * create Time at 2022/6/13 15:47
 *
 * @author alice.ruge
 * @since 0.0.7
 */
@Getter
@Setter
public class TreeNodeDTO {

    /**
     * ID
     */
    private Long id;
    /**
     * 父级ID
     */
    private Long parentId;
    /**
     * 子节点列表
     */
    private List<? extends TreeNodeDTO> childrenList;
}
