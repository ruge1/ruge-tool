package com.ruge.tool.yaml;

import com.ruge.tool.file.FileTool;
import org.apache.commons.io.FileUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * description: YamlTool
 * create time at 2022/12/2 18:47
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public class YamlTool {


    public static void main(String[] args) throws IOException {
        List<String> list = new ArrayList<>();
        String path = "E:\\work\\avris\\uat_nacos_config_export_20221203203219\\DEFAULT_GROUP";
        List<File> result = new ArrayList<>();
        FileTool.listFile(new File(path), result);
        for (int i = 0; i < result.size(); i++) {
            Yaml yaml = new Yaml();
//        String filePath = "E:\\work\\avris\\prod_nacos_config_export_20221117084928_public\\DEFAULT_GROUP/application-dev.yml";
            String filePath = result.get(i).getAbsolutePath();
            System.out.println(filePath);
            list.add(filePath);
            try (InputStream inputStream = new FileInputStream(filePath)) {
                Object object = yaml.load(inputStream);
                List<String> resultList = travelRootWithResult(object);
                System.out.println(resultList);
                resultList = resultList.stream().filter(e -> !e.contains("jasypt")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("spring")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("mybatis")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("swagger")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("security")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("cms.content-type")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("management")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("feign")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("ribbon")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("logging")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("gray")).collect(Collectors.toList());
                resultList = resultList.stream().filter(e -> !e.contains("xxl")).collect(Collectors.toList());


                resultList = resultList.stream().filter(e -> !e.contains("appointment")).collect(Collectors.toList());

//                resultList = resultList.stream().filter(e -> e.contains("xxl")).collect(Collectors.toList());

                list.addAll(resultList);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        FileUtils.writeLines(new File("E:\\work\\avris\\demo.txt"), list);
    }

    private static List<String> travelRootWithResult(Object object) {
        List<String> resultList = new ArrayList<>();
        if (object instanceof LinkedHashMap) {
            LinkedHashMap map = (LinkedHashMap) object;
            Set<Object> keySet = map.keySet();
            for (Object key : keySet) {
                List<String> keyList = new ArrayList<>();
                keyList.add((String) key);
                travelTreeNode(map.get(key), keyList, resultList);
            }
        }
        return resultList;
    }


    private static void travelTreeNode(Object obj, List<String> keyList, List<String> resultList) {
        if (obj instanceof LinkedHashMap) {
            LinkedHashMap linkedHashMap = (LinkedHashMap) obj;
            linkedHashMap.forEach((key, value) -> {
                if (value instanceof LinkedHashMap) {
                    keyList.add((String) key);
                    travelTreeNode(value, keyList, resultList);
                    keyList.remove(keyList.size() - 1);
                } else {
                    StringBuilder result = new StringBuilder();
                    for (String strKey : keyList) {
                        result.append(strKey).append(".");
                    }
                    result.append(key).append("=").append(value);
                    System.out.println(result);
                    resultList.add(result.toString());
                }
            });
        } else {
            StringBuilder result = new StringBuilder();
            result.append(keyList.get(0)).append("=").append(obj);
            System.out.println(result);
            resultList.add(result.toString());
        }
    }
}
