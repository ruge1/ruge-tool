package com.ruge.tool.token;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.ruge.tool.json.JsonTool;
import com.ruge.tool.map.MapTool;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * token 工具类
 *
 * @author ruge.wu
 * @since 2021-10-15 08:15
 **/
public class JwtTokenTool {
    /**
     * 秘钥
     * 用户
     */
    public static final String SECRETUSER = "5pil6aOO5YaN576O5Lmf5q+U5LiN5LiK5bCP6ZuF55qE56yR";
    //    private static final String SECRETUSER = "token!Q@W#E$RFOXGOING";
    //
    /**
     * 秘钥
     * 管理员
     */
    public static final String SECRETADMIN = "ADMIN5pil6aOO5YaN576O5Lmf5q+U5LiN5LiK5bCP6ZuF55qE56yR";


    /**
     * 生成令牌-管理员
     *
     * @param payload payload
     * @param secret  秘钥
     * @return 令牌
     * @throws Exception {@link Exception}
     */
    public static String generateTokenAdmin(Object payload, String secret) throws Exception {
        return generateToken(payload, secret);
    }

    /**
     * 生成令牌-普通用户
     *
     * @param payload payload
     * @param secret  秘钥
     * @return 令牌
     * @throws Exception {@link Exception}
     */
    public static String generateTokenUser(Object payload, String secret) throws Exception {
        return generateToken(payload, secret);
    }

    /**
     * 生成令牌
     *
     * @param payload Object参数
     * @param secret  秘钥
     * @return 令牌
     * @throws Exception 异常
     */
    public static String generateToken(Object payload, String secret) throws Exception {
        JWTCreator.Builder builder = JWT.create();
        //payload
        Map<String, String> map = JsonTool.getObjToStringMap(payload);
        map.forEach(builder::withClaim);
        Calendar instance = Calendar.getInstance();
        //默认7天过期
        instance.add(Calendar.DATE, 7);
        //指定令牌的过期时间
        builder.withExpiresAt(instance.getTime());
        return builder.sign(Algorithm.HMAC256(secret));
    }


    /**
     * 解密JWT令牌
     *
     * @param token  token
     * @param secret 秘钥
     * @return 解密后的token
     */
    public static Map<String, Object> parseToken(String token, String secret) {
        //以Bearer开头处理
        String keyword = "Bearer";
        if (token.startsWith(keyword)) {
            token = token.substring(6).trim();
        }
        //如果有任何验证异常，此处都会抛出异常
        Map<String, Claim> claims = JWT.require(Algorithm.HMAC256(secret))
                .build()
                .verify(token)
                .getClaims();
        Map<String, Object> build = MapTool.newHashMap();
        claims.forEach((k, v) -> build.put(k, v));
        return build;
    }

    public static void main(String[] args) throws Exception {
        Map<String, Object> map = new HashMap<>(0);
        map.put("aid", "1");
        map.put("nickName", "张三");
        String key = "asd";
        String tokenAdmin = generateTokenAdmin(map, SECRETUSER);
        System.out.println(tokenAdmin);


        Map<String, Object> parseToken = parseToken(tokenAdmin, SECRETUSER);
        System.out.println(parseToken);
    }
}
