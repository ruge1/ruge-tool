package com.ruge.tool.http;

import com.ruge.tool.json.JsonTool;
import lombok.Data;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ProtocolVersion;

import java.util.Locale;

/**
 * description: HttpResponse
 * create time at 2022/11/25 16:25
 *
 * @author alice.ruge
 * @since 0.0.3
 */
@Data
public class HttpResponse<T> {
    private final int code;
    private final HttpEntity entity;
    private final T data;
    private final ProtocolVersion version;
    private final Locale locale;
    private final String reasonPhrase;

    public HttpResponse(int code, HttpEntity entity, T data, ProtocolVersion version, Locale locale, String reasonPhrase) {
        this.code = code;
        this.entity = entity;
        this.data = data;
        this.version = version;
        this.locale = locale;
        this.reasonPhrase = reasonPhrase;
    }

    public static <T> HttpResponse<T> build(ClassicHttpResponse response, T data) {
        response.getEntity();
        return new HttpResponse<T>(response.getCode(), response.getEntity(), data, response.getVersion(), response.getLocale(), response.getReasonPhrase());
    }


    @Override
    public String toString() {
        return JsonTool.getObjToJson(this);
    }
}
