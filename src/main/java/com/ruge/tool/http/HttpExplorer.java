package com.ruge.tool.http;

import com.ruge.tool.json.JsonTool;
import com.ruge.tool.str.StringTool;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.io.entity.StringEntity;

import java.io.IOException;

/**
 * description: HttpExplorer
 * create time at 2022/11/25 16:24
 *
 * @author alice.ruge
 * @since 0.0.3
 */
@Slf4j
public class HttpExplorer<T> {
    private CloseableHttpClient httpClient = null;
    private HttpConfig config;

    public HttpExplorer(HttpConfig config) {
        if (httpClient == null) {
            synchronized (HttpExplorer.class) {
                this.config = config;
                httpClient = config.getBuilder().build();
            }
        }
    }

    /**
     * 向指定地址发送Get请求
     *
     * @param uri 目标地址
     * @return 响应信息
     */
    public HttpResponse<String> get(String uri) {
        HttpResponse<String> handler = null;
        try {
            CloseableHttpResponse response = this.httpClient.execute(config.buildGet(uri));
            handler = HttpResponseHandler.ofString(config.getResCode()).handler(response, this);
            return handler;
        } catch (IOException e) {
            log.error("HttpExplorer is error url:{},headers:{},response:{},ex:{}", uri, config.getHeaderMap(), JsonTool.getObjToJson(handler), e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * 向指定地址发送Get请求
     *
     * @param uri 目标地址
     * @return 响应信息
     */
//    public HttpResponse<File> getFile(String uri, String path){
//        try {
//            CloseableHttpResponse response = this.httpClient.execute(config.buildGet(uri));
//            return HttpResponseHandler
//                    .ofFile(Path.of(path), StandardCopyOption.REPLACE_EXISTING)
//                    .handler(response,this);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
    public HttpResponse<String> post(String uri) {
        return post(uri, null);

    }

    /**
     * 向指定地址发送Post请求
     *
     * @param uri 目标地址
     * @return 响应信息
     */
    public HttpResponse<String> post(String uri, Object body) {
        try {
            HttpPost httpPost = config.buildPost(uri);
            if (StringTool.isNotBlank(body)) {
                StringEntity stringEntity = new StringEntity(JsonTool.getObjToJson(body), ContentType.APPLICATION_JSON);
                httpPost.setEntity(stringEntity);

            }
            CloseableHttpResponse response = this.httpClient.execute(httpPost, new HttpClientContext());
            return HttpResponseHandler.ofString(config.getResCode()).handler(response, this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 创建按构造器，用于构造HttpClient配置
     *
     * @return
     */
    public static HttpConfig builder() {
        return new HttpConfig();
    }
}
