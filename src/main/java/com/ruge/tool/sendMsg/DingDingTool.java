package com.ruge.tool.sendMsg;

import cn.hutool.core.map.MapBuilder;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.ruge.tool.constants.Constants;
import com.ruge.tool.json.JsonTool;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * DingDingTool  
 * @author admin
 * @version 2024/07/26 08:20
 **/
@Slf4j
public class DingDingTool {

    public static void sendMsg(String msg) {
        Map body = new HashMap<>();
        body.put("msgtype", "markdown");
        body.put("at", MapBuilder.create().put("atMobiles", Arrays.asList("13298899809")).put("isAtAll", false).build());
        body.put("markdown", MapBuilder.create().put("title", "自动签到").put("text", msg).build());
        HttpResponse response = HttpUtil.createPost("https://oapi.dingtalk.com/robot/send?access_token=" + Constants.TOKEN_DINGDING).body(JsonTool.getObjToJson(body)).execute();
        log.info("钉钉消息推送:" + response);
    }
}
