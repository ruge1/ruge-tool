/**
 * IO流工具类,用于处理读,写和拷贝,这些方法基于 InputStream, OutputStream, Reader 和 Writer工作
 * commons-io:commons-io
 */
package com.ruge.tool.io;