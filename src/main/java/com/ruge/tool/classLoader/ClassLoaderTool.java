package com.ruge.tool.classLoader;

import com.ruge.tool.str.StringTool;

import java.net.URL;

;

/**
 * description: ClassLoaderTool
 * 双亲委派工作原理
 * 【1】如果一个类加载器收到一个加载请求，他不会先去自己加载，而是把这个请求委托给父类的加载器去执行
 * 【2】如果父类的加载器还存在父类，则会进一步向上委托，请求最终会到达顶层的启动类加载器
 * 【3】如果父类加载器可以完成类加载，则正常返回，如果无法完成加载，子类加载器才会尝试自己去加载
 * 这就是双亲委派 https://blog.csdn.net/codeyanbao/article/details/82875064[通俗易懂的双亲委派机制]
 * <p>
 * create time at 2022/11/25 0:10
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public class ClassLoaderTool {
    /**
     * 获取 Bootstrap 能够加载的 api 路径
     *
     * @return {@link URL}
     */
    public static URL[] getBootstrapURLs() {
        return sun.misc.Launcher.getBootstrapClassPath().getURLs();
    }

    /**
     * 获取拓展类加载器
     *
     * @return {@link String}
     */
    public static String[] getExtDirs() {
        String dirs = System.getProperty("java.ext.dirs");
        if (StringTool.isNotBlank(dirs)) {
            return dirs.split(";");
        }
        return null;
    }
}
