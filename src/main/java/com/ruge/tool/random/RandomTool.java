package com.ruge.tool.random;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author ruge.wu
 * @since 2021/12/24 11:09
 */
public class RandomTool {
    /**
     *获取随机数生成器对象
     * @return {@link  ThreadLocalRandom}
     */
    public static ThreadLocalRandom getRandom() {
        return ThreadLocalRandom.current();
    }

    public static int nextInt() {
        return getRandom().nextInt();
    }

    /**
     * 获得正整数的随机数
     * @param bound 最大值
     * @return 随机数
     */
    public static int nextInt( int bound) {
        return getRandom().nextInt(0,bound);
    }

    /**
     * 获得指定范围内的随机数
     * @param origin 最小值
     * @param bound 最大值
     * @return 随机数
     */
    public static int nextInt(int origin, int bound) {
        return getRandom().nextInt(origin,bound);
    }

    /**
     * 获得指定范围内的随机数[min, max)
     *
     * @param min 最小数（包含）
     * @param max 最大数（不包含）
     * @return 随机数
     */
    public static long nextLong(long min, long max) {
        return getRandom().nextLong(min, max);
    }

    /**
     * 获得随机数
     *
     * @return 随机数
     */
    public static long nextLong() {
        return getRandom().nextLong();
    }

    /**
     * 获得指定范围内的随机数 [0,limit)
     *
     * @param limit 限制随机数的范围，不包括这个数
     * @return 随机数
     */
    public static long nextLong(long limit) {
        return getRandom().nextLong(limit);
    }
}
