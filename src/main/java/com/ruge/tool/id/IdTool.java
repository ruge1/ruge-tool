package com.ruge.tool.id;

import cn.hutool.core.util.IdUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>名称：IdWorker.java</p>
 * <p>描述：分布式自增长ID</p>
 * <pre>
 *     Twitter的 Snowflake　JAVA实现方案
 * </pre>
 * 核心代码为其IdWorker这个类实现，其原理结构如下，我分别用一个0表示一位，用—分割开部分的作用：
 * 1||0---0000000000 0000000000 0000000000 0000000000 0 --- 00000 ---00000 ---000000000000
 * 在上面的字符串中，第一位为未使用（实际上也可作为long的符号位），接下来的41位为毫秒级时间，
 * 然后5位datacenter标识位，5位机器ID（并不算标识符，实际是为线程标识），
 * 然后12位该毫秒内的当前毫秒内的计数，加起来刚好64位，为一个Long型。
 * 这样的好处是，整体上按照时间自增排序，并且整个分布式系统内不会产生ID碰撞（由datacenter和机器ID作区分），
 * 并且效率较高，经测试，snowflake每秒能够产生26万ID左右，完全满足需要。
 * <p>
 * 64位ID (42(毫秒)+5(机器ID)+5(业务编码)+12(重复累加))
 *
 * @author ruge.wu
 * @since 2021-10-18 15:10
 **/
public class IdTool extends IdUtil {

    /**
     * 生成订单编号
     * <p>
     * String qsc = IdTool.genOrderId("QSCF", null);
     * System.out.println(qsc);
     * qsc = IdTool.genOrderId("QSCF", qsc);
     * System.out.println(qsc);
     * <p>
     * QSCF20220614001
     * QSCF20220614002
     *
     * @param prefix 前缀
     * @param bid    当前编号,也就是业务号
     * @return 订单编号
     */
    public static String genOrderId(String prefix, String bid) {
        //格式说明 CODE20201111xxx CODE+当前年月日+编号（具体长度看需求）
        //当前时间编码
        Date date = new Date();
        String bidDate = new SimpleDateFormat("yyyyMMdd").format(date);
        // 在数据表中查到了，说明现在这个订单不是今天的第一单
        if (bid != null) {
            // 取出后三位数，也就是自动生成的三位数 001
            bid = bid.substring(prefix.length() + bidDate.length());
            int num = Integer.parseInt(bid);
            num++;  // 加1
            // 数字10
            Integer num10 = 10;
            // 数字100
            Integer num100 = 100;
            if (num < num10) {
                //%03d 只是三位，不足补0
                String bidNum = String.format("%03d", num);
                return prefix + bidDate + bidNum;
            } else if (num < num100) {
                //num<100,说明是两位数，前面要补一个0
                String bidNum = String.format("%03d", num);
                return prefix + bidDate + bidNum;
            } else {
                String bidNum = String.valueOf(num);
                return prefix + bidDate + bidNum;
            }
        } else {
            int number = 1;
            String bidNum = "00" + number;
            return prefix + bidDate + bidNum;
        }
    }
}
