package com.ruge.tool.lambda;

import java.io.Serializable;

/**
 * @author ruge.wu
 * @since 2021/12/24 16:01
 */
@FunctionalInterface
public interface R<T> extends Serializable {
    /**
     * lambda 泛型
     *
     * @param source 泛型
     * @return 泛型对象
     */
    Object get(T source);
}
