package com.ruge.tool.seckill;

/**
 * 秒杀 工具类
 *
 * @author ruge.wu
 * @since 2021-10-18 14:07
 **/
public interface Seckill {

    /**
     * 1 递减库存,利用mysql行级锁 控制超卖
     *  UPDATE tb_sku SET seckill_num=seckill_num-#{count} WHERE id=#{id} AND islock=1 AND seckill_num  %gt;  =#{count}
     * 1.1 库存不足需要报错
     *
     * @param id    商品id
     * @param count 商品递减数量
     */
    void dCount(String id, Integer count);

    /**
     * 2 库存递减成功后执行下单
     */
    void addOrder();

    /**
     * 3 下单失败 需要实现分布式事务
     */
    void op3();

    /**
     * 4 下单成功后 要记录用户抢单信息，24小时内 不允许抢购该商品
     */
    void op4();

    /**
     * 5 抢单中，有可能抢购的商品变成热点商品，此时应该走排队的方式抢单
     */
    void op5();

}
