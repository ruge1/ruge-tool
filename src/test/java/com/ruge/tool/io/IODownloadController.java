package com.ruge.tool.io;

import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URL;

/**
 * description
 * create Time at 2022/6/20 14:25
 *
 * @author alice.ruge
 * @since 0.0.7
 */
@RestController
public class IODownloadController {

    @SneakyThrows
    @GetMapping("download1")
    public void getAuthorizedPhoto(HttpServletResponse response) {
        URL url = new URL("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic2.52pk.com%2Ffiles%2F100721%2F126_134543_1_lit.jpg&refer=http%3A%2F%2Fpic2.52pk.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1658297478&t=4fd112ac2272a137ff88346f03456b57");
        IOTool.copy(url, response.getOutputStream());
    }
}
