package com.ruge.tool.io;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class IOToolTest {
    @Test
    @SneakyThrows
    public void testCopy() {
        URL url = new URL("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic2.52pk.com%2Ffiles%2F100721%2F126_134543_1_lit.jpg&refer=http%3A%2F%2Fpic2.52pk.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1658297478&t=4fd112ac2272a137ff88346f03456b57");
        File file = new File("E:\\work\\a.jpg");
        IOTool.copy(url,file);
    }
}