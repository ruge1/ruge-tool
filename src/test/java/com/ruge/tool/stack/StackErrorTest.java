package com.ruge.tool.stack;

import com.ruge.tool.map.MapTool;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;

/**
 * description:
 * create time at 2022/12/12 17:47
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public class StackErrorTest {

    private static int i = 0;

    /**
     * 线程的堆栈大小超过分配的额度,则会抛出{@link StackOverflowError}
     * 增加线程堆栈大小解决
     * -Xss
     * <p>
     * -Xss2m  这会将线程的堆栈大小设置为2 mb
     * 默认值 11300
     * -Xss512k
     */
    @Test
    public void testStackOverflowError() throws MalformedURLException {
        String link = "https://operations-center-web-uat.faw-vw.com/questionnairepublish/mobileDetail?id=2000274";
//        link = link.replaceAll("#", "");
        System.out.println(MapTool.getUrlParams(link));
    }

    public static void main(String[] args) {
        demo();
    }

    public static void demo() {
        System.out.println(i);
        i++;
        demo();

    }
}
