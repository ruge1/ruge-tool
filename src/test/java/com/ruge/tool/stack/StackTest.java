package com.ruge.tool.stack;

/**
 * description:
 * create time at 2022/12/20 23:43
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public class StackTest {
    public void add() {
        // 第一类问题
        int i1 = 10;
        i1++;
        System.out.println(i1);
        int i2 = 10;
        ++i2;

        System.out.println(i2);
        // 第二类问题
        int i3 = 10;
        int i4 = i3++;
        System.out.println(i3);
        System.out.println(i4);

        int i5 = 10;
        int i6 = ++i5;
        System.out.println(i5);
        System.out.println(i6);
        // 第三类问题
        int i7 = 10;
        i7 = i7++;
        System.out.println(i7);
        int i8 = 10;
        i8 = ++i8;
        System.out.println(i8);
        // 第四类问题
        int i9 = 10;
        int i10 = i9++ + ++i9;
        System.out.println(i9);
        System.out.println(i10);
    }
}
