package com.ruge.tool.redisson;

import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class RedissonDistributedLockerTest {

    private RedissonClient redissonClient;

    private RedissonDistributedLocker redissonDistributedLocker;
    public static final String rLockName1 = "rLock1";
    public static final String rLockName2 = "rLock2";

    @BeforeAll
    public void before() {
        Config config = new Config();
        config.useSingleServer()
                .setPassword("arcfox890")
                .setAddress("redis://192.168.10.74:6382");

        redissonClient = Redisson.create(config);
        redissonDistributedLocker = new RedissonDistributedLocker(redissonClient);
    }

    @Test
    @SneakyThrows
    public void lock() {
        redissonDistributedLocker.lock(rLockName1);
        redissonDistributedLocker.unlock(rLockName1);
    }

    @Test
    public void testLock() {
    }

    @Test
    public void testLock1() {
    }

    @Test
    public void tryLock() {
    }

    @Test
    public void unlock() {
    }

    @Test
    public void testUnlock() {
    }

    @AfterAll
    public void testAfter() {

    }
}