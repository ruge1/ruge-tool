package com.ruge.tool.rsa;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class RsaToolTest {

    @Test
    void encryptByPrivateKey() {
    }

    @Test
    void generateKey() {
        Map<String, String> stringStringMap = RsaTool.generateKey();
        System.out.println(stringStringMap);
    }

    @Test
    void encryptByPublicKey() throws Exception {
        String key = RsaTool.encryptByPublicKey("13298899809", "-----BEGIN PUBLIC KEY-----MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDc+CZK9bBA9IU+gZUOc6FUGu7yO9WpTNB0PzmgFBh96Mg1WrovD1oqZ+eIF4LjvxKXGOdI79JRdve9NPhQo07+uqGQgE4imwNnRx7PFtCRryiIEcUoavuNtuRVoBAm6qdB0SrctgaqGfLgKvZHOnwTjyNqjBUxzMeQlEC2czEMSwIDAQAB-----END PUBLIC KEY-----");
        System.out.println(key);
    }

    @Test
    void decryptByPrivateKey() {
    }

    @Test
    void testEncryptByPrivateKey() {
    }

    @Test
    void decryptByPublicKey() {
    }

    @Test
    void sign() {
    }

    @Test
    void verify() {
    }
}