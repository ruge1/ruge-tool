package com.ruge.tool.http;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * description:
 *
 * @author alice.ruge
 * date 2022/5/10 17:18
 * @since 0.0.1
 */
@Data
public class NbaPlayer {
    /**
     * 球队名称
     */
    private String teamName;
    /**
     * 球员名称
     */
    private String playerName;
    /**
     * 位置
     */
    private List<Integer> position = new ArrayList<>();
    /**
     * 球员得分
     */
    private Integer point;
    /**
     * 总篮板
     */
    private Integer rebounds;
    /**
     * 助攻
     */
    private Integer assists;
    /**
     * 失误
     */
    private Integer turnovers;
    /**
     * 抢断
     */
    private Integer steals;
    /**
     * 盖帽
     */
    private Integer blocks;
    /**
     * nba篮球大师得分
     * 得分*0.7 + 篮板*1 + 助攻*1.1 + 抢断*1.2 + 盖帽*1 - 失误*0.5
     */
    private Double gameCount;

    public Double getGameCount() {
        double one = getPoint() * 0.7 + getRebounds() * 1 + getAssists() * 1.1 + getSteals() * 1.2 + blocks - turnovers * 0.5;
        String str = String.format("%.2f", one);
        return Double.parseDouble(str);
    }

    /**
     * 薪资
     */
    private Integer salary;

    @Override
    public String toString() {
        return teamName + " " + playerName + ":" + getGameCount();
    }
}
