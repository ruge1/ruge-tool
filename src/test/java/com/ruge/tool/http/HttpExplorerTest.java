package com.ruge.tool.http;

import com.aliyun.tea.TeaException;
import com.ruge.tool.sign.SignTool;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
class HttpExplorerTest {

    @Test
    void get() {
        //网页资源访问
        HttpResponse<String> stringHttpResponse = HttpExplorer.builder()
                .build()
                .get("http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/index.html");
        System.out.println(stringHttpResponse.toString());
    }

    /**
     * 使用 Token 初始化账号Client
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dingtalkrobot_1_0.Client createClient() throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkrobot_1_0.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        com.aliyun.dingtalkrobot_1_0.Client client = createClient();
        com.aliyun.dingtalkrobot_1_0.models.OrgGroupSendHeaders orgGroupSendHeaders = new com.aliyun.dingtalkrobot_1_0.models.OrgGroupSendHeaders();
        orgGroupSendHeaders.xAcsDingtalkAccessToken = "0f08951cfd74612cace11f3282cd53449b10d9964fc55a7e2bf5d7911d3fb4e0";
        com.aliyun.dingtalkrobot_1_0.models.OrgGroupSendRequest orgGroupSendRequest = new com.aliyun.dingtalkrobot_1_0.models.OrgGroupSendRequest()
                .setMsgParam("error {\"content\":\"今天吃肘子\"}")
                .setMsgKey("sampleText")
                .setOpenConversationId("cid6KeBBLoveMJOGXoYKF5x7EeiodoA==")
                .setRobotCode("dingue4kfzdxbynxxxxxx");
        try {
            client.orgGroupSendWithOptions(orgGroupSendRequest, orgGroupSendHeaders, new com.aliyun.teautil.models.RuntimeOptions());
        } catch (TeaException err) {
            err.printStackTrace();
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        } catch (Exception _err) {
            _err.printStackTrace();
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        }
    }

    @Test
    void post() {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost("https://oapi.dingtalk.com/robot/send?access_token=0f08951cfd74612cace11f3282cd53449b10d9964fc55a7e2bf5d7911d3fb4e0");
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("account", "mama.bit"));
            nvps.add(new BasicNameValuePair("address", ""));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps));
            CloseableHttpResponse response = httpclient.execute(httpPost);
            HttpResponse<String> result = HttpResponseHandler.ofString().handler(response, null);
            System.out.println(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    @Test
    void build_PROD_2() {
        // 大五座揽巡 数据详情
        String envUrl = "https://operations-center-web.faw-vw.com/cdp/external/";
        String serverUrl = "marketing-adapter/oc/dashboard/v1/query-content-display?id=27294&contentId=1000401&startDate=2022-11-08&endDate=2022-11-14";
        String appkey = "be7311d7578c4aa39989c7651ce52e47";
        String secretKey = "95822AF4F2DBEBCA1F0E230D5DD5048E";
        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        HttpResponse stringHttpResponse = HttpExplorer.builder().build().get(buildTimaUrl);
        System.out.println(stringHttpResponse.getData());
    }


    @SneakyThrows
    @Test
    public void build_PROD_3() {
        // 大五座揽巡 数据详情
        String envUrl = "http://192.168.175.34/";
        String serverUrl = "one-app/general/manage/v1/integration/operation/data_active?id=27651";
        String appkey = "be7311d7578c4aa39989c7651ce52e47";
        String secretKey = "95822AF4F2DBEBCA1F0E230D5DD5048E";

        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        HttpResponse stringHttpResponse = HttpExplorer.builder().build().get(buildTimaUrl);
        System.out.println(stringHttpResponse.toString());
    }
}