package com.ruge.tool.http;

import com.ruge.tool.coll.CollTool;
import com.ruge.tool.json.JsonTool;
import com.ruge.tool.map.MapTool;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * description
 * create Time at 2022/7/14 17:50
 *
 * @author alice.ruge
 * @since 0.0.7
 */
public class HttpToolLaGou {
    @Test
    public void test专栏() {

        List<Map<String, String>> courseLists = new ArrayList<>();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("Accept", "application/json, text/plain, */*"));
        params.add(new BasicNameValuePair("Accept-Encoding", "gzip, deflate, br"));
        params.add(new BasicNameValuePair("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2"));
        params.add(new BasicNameValuePair("Connection", "keep-alive"));
        params.add(new BasicNameValuePair("Cookie", "EDUJSESSIONID=ABAAABAAAECABEHBF92E11ECF3EE068BB3ADBEA2E831342; X_HTTP_TOKEN=42daf4b72327b2819154977561bf5e71415983ed09; user-finger=91cb22a0fd2a6790813c3cb7455b9a0d; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22181fc0d0d4962-0a2652a83f24d38-c575422-1327104-181fc0d0d4a16%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E8%87%AA%E7%84%B6%E6%90%9C%E7%B4%A2%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC%22%2C%22%24latest_referrer%22%3A%22https%3A%2F%2Fwww.baidu.com%2Flink%22%2C%22%24os%22%3A%22Windows%22%2C%22%24browser%22%3A%22Firefox%22%2C%22%24browser_version%22%3A%22102.0%22%7D%2C%22%24device_id%22%3A%22181fc0d0d4962-0a2652a83f24d38-c575422-1327104-181fc0d0d4a16%22%7D; sajssdk_2015_cross_new_user=1; sensorsdata2015session=%7B%7D"));
        params.add(new BasicNameValuePair("edu-referer", "https://edu.lagou.com/"));
        params.add(new BasicNameValuePair("Host", "gate.lagou.com"));
        params.add(new BasicNameValuePair("Origin", "https://edu.lagou.com"));
        params.add(new BasicNameValuePair("Referer", "https://edu.lagou.com/"));
        params.add(new BasicNameValuePair("Sec-Fetch-Dest", "empty"));
        params.add(new BasicNameValuePair("Sec-Fetch-Mode", "cors"));
        params.add(new BasicNameValuePair("Sec-Fetch-Site", "same-site"));
        params.add(new BasicNameValuePair("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0"));
        params.add(new BasicNameValuePair("X-L-REQ-HEADER", "{\"deviceType\":1}"));
        String result = HttpExplorer.builder().setPairs(params).build().get("https://gate.lagou.com/v1/neirong/edu/homepage/getCourseListV2?isPc=true&t=" + System.currentTimeMillis()).getData().toString();
        Map<String, Object> map = JsonTool.getJsonToMap(result);
        Map<String, Object> content = JsonTool.getObjToMap(map.get("content"));
        List<Map<String, Object>> maps = JsonTool.getObjToMaps(content.get("contentCardList"));
        for (Map<String, Object> objectMap : maps) {
            List<Map<String, Object>> courseList = JsonTool.getObjToMaps(objectMap.get("courseList"));
            for (Map<String, Object> stringObjectMap : courseList) {
                Map<String, String> courseMap = MapTool.newHashMap("", "");
                courseMap.put("title", String.valueOf(stringObjectMap.get("title")));
                courseMap.put("courseType", String.valueOf(stringObjectMap.get("courseType")));
                courseLists.add(courseMap);
            }
        }
        CollTool.reverse(courseLists);
        for (int i = 0; i < courseLists.size(); i++) {
            System.out.println(i + 1 + "===" + courseLists.get(i));
        }
    }

    @Test
    public void test掘金() {

        List<Map<String, String>> courseLists = new ArrayList<>();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("Accept", "application/json, text/plain, */*"));
        params.add(new BasicNameValuePair("Accept-Encoding", "gzip, deflate, br"));
        params.add(new BasicNameValuePair("Accept-Language", "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2"));
        params.add(new BasicNameValuePair("Connection", "keep-alive"));
        params.add(new BasicNameValuePair("Cookie", "EDUJSESSIONID=ABAAABAAAECABEHBF92E11ECF3EE068BB3ADBEA2E831342; X_HTTP_TOKEN=42daf4b72327b2819154977561bf5e71415983ed09; user-finger=91cb22a0fd2a6790813c3cb7455b9a0d; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22181fc0d0d4962-0a2652a83f24d38-c575422-1327104-181fc0d0d4a16%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E8%87%AA%E7%84%B6%E6%90%9C%E7%B4%A2%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC%22%2C%22%24latest_referrer%22%3A%22https%3A%2F%2Fwww.baidu.com%2Flink%22%2C%22%24os%22%3A%22Windows%22%2C%22%24browser%22%3A%22Firefox%22%2C%22%24browser_version%22%3A%22102.0%22%7D%2C%22%24device_id%22%3A%22181fc0d0d4962-0a2652a83f24d38-c575422-1327104-181fc0d0d4a16%22%7D; sajssdk_2015_cross_new_user=1; sensorsdata2015session=%7B%7D"));
        params.add(new BasicNameValuePair("edu-referer", "https://edu.lagou.com/"));
        params.add(new BasicNameValuePair("Host", "gate.lagou.com"));
        params.add(new BasicNameValuePair("Origin", "https://edu.lagou.com"));
        params.add(new BasicNameValuePair("Referer", "https://edu.lagou.com/"));
        params.add(new BasicNameValuePair("Sec-Fetch-Dest", "empty"));
        params.add(new BasicNameValuePair("Sec-Fetch-Mode", "cors"));
        params.add(new BasicNameValuePair("Sec-Fetch-Site", "same-site"));
        params.add(new BasicNameValuePair("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0"));
        params.add(new BasicNameValuePair("X-L-REQ-HEADER", "{\"deviceType\":1}"));
        String result = HttpExplorer.builder().setPairs(params).build().get("https://gate.lagou.com/v1/neirong/edu/homepage/getCourseListV2?isPc=true&t=" + System.currentTimeMillis()).getData().toString();
        Map<String, Object> map = JsonTool.getJsonToMap(result);
        Map<String, Object> content = JsonTool.getObjToMap(map.get("content"));
        List<Map<String, Object>> maps = JsonTool.getObjToMaps(content.get("contentCardList"));
        for (Map<String, Object> objectMap : maps) {
            List<Map<String, Object>> courseList = JsonTool.getObjToMaps(objectMap.get("courseList"));
            for (Map<String, Object> stringObjectMap : courseList) {
                Map<String, String> courseMap = MapTool.newHashMap("", "");
                courseMap.put("title", String.valueOf(stringObjectMap.get("title")));
                courseMap.put("courseType", String.valueOf(stringObjectMap.get("courseType")));
                courseLists.add(courseMap);
            }
        }
        CollTool.reverse(courseLists);
        for (int i = 0; i < courseLists.size(); i++) {
            System.out.println(i + 1 + "===" + courseLists.get(i));
        }
    }
}
