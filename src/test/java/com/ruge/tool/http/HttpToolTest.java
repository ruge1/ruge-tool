package com.ruge.tool.http;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruge.tool.json.JsonTool;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ruge.wu
 * @since 2022/1/26 12:40
 */
@Slf4j
public class HttpToolTest {

    public static final String URL = "https://service-8dtih7or-1256532032.ap-beijing.apigateway.myqcloud.com/test/mosc-admin/one-app-gateway/mosc/one-app/general/manage/v1/";
    public static final String UAT = "https://one-app-admin-9528-uat2.cdp5-dev.nxengine.com/stage-api/web/";
    public static final String PROD = "https://one-app-admin-html.faw-vw.com/prod-api/web/";

    @Test
    public void nba() {
        String date = "2022-06-14";
        String callBack = "jQuery1113045061788176702944_1652171530674";
        List<NbaPlayer> nbaPlayers = new ArrayList<>();
        String url = "https://slamdunk.sports.sina.com.cn/api?p=radar&callback=" + callBack + "&s=schedule&a=date_span&date=" + date + "&span=6&_=1652171530679";
        String result = HttpExplorer.builder().build().get(url).getData().toString();
        result = result.replace("try{" + callBack + "(", "");
        result = result.replace(");}catch(e){};", "");
        Map<String, Object> jsonToMap = JsonTool.getJsonToMap(result);
        jsonToMap = JsonTool.getObjToMap(jsonToMap.get("result"));
        jsonToMap = JsonTool.getObjToMap(jsonToMap.get("data"));
        List<Map<String, Object>> mapList = JsonTool.getObjToMaps(jsonToMap.get("matchs"));
        List mids = mapList.stream()
                .map(m -> m.get("mid"))
                .collect(Collectors.toList());
        for (Object mid : mids) {
            String detailUrl = "https://slamdunk.sports.sina.com.cn/api?p=radar&callback=" + callBack + "&p=radar&s=summary&a=game_player&mid=" + mid + "&_=1652172064531";
            String detailResult = HttpExplorer.builder().build().get(detailUrl).getData().toString();
            detailResult = detailResult.replace("try{" + callBack + "(", "");
            detailResult = detailResult.replace(");}catch(e){};", "");
            Map<String, Object> jsonToDetailMap = JsonTool.getJsonToMap(detailResult);
            jsonToDetailMap = JsonTool.getObjToMap(jsonToDetailMap.get("result"));
            if (jsonToDetailMap.get("data") instanceof JSONArray) {
                continue;
            }
            JSONObject jsonObject = (JSONObject) jsonToDetailMap.get("data");
            if (jsonObject.size() > 0) {
                jsonToDetailMap = JsonTool.getObjToMap(jsonToDetailMap.get("data"));
                for (String key : jsonToDetailMap.keySet()) {
                    Map<String, Object> away = (Map<String, Object>) jsonToDetailMap.get(key);
                    List<Map<String, Object>> players = JsonTool.getObjToMaps(away.get("players"));
                    away = JsonTool.getObjToMap(away.get("total"));
                    for (Map<String, Object> player : players) {
                        NbaPlayer nbaPlayer = new NbaPlayer();
                        nbaPlayer.setTeamName((String) away.get("team_name"));
                        nbaPlayer.setPlayerName(player.get("first_name") + "-" + player.get("last_name"));
//                        nbaPlayer.setPosition((String) player.get("position"));
                        nbaPlayer.setPoint((Integer) player.get("points"));
                        nbaPlayer.setRebounds((Integer) player.get("rebounds"));
                        nbaPlayer.setAssists((Integer) player.get("assists"));
                        nbaPlayer.setTurnovers((Integer) player.get("turnovers"));
                        nbaPlayer.setSteals((Integer) player.get("steals"));
                        nbaPlayer.setBlocks((Integer) player.get("blocks"));
                        nbaPlayers.add(nbaPlayer);
                    }
                }
            }
        }
        nbaPlayers.stream().sorted(Comparator.comparing(NbaPlayer::getGameCount).reversed()).forEach(System.out::println);
        System.out.println(nbaPlayers.stream().sorted(Comparator.comparing(NbaPlayer::getGameCount).reversed()).limit(5).mapToDouble(NbaPlayer::getGameCount).sum());
    }


    @Test
    public void post() {
        String post = HttpExplorer.builder().build().post("https://blog.csdn.net/super_scan/article/details/120415552").getData().toString();
        System.out.println(post);
    }
}

