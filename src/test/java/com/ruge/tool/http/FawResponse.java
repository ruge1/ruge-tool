package com.ruge.tool.http;

import lombok.Data;

/**
 * description: FawResponse
 * create time at 2022/11/26 0:01
 *
 * @author alice.ruge
 * @since 0.0.3
 */

@Data
public class FawResponse {
    private String returnStatus;
    private Long serverTime;
    private String requestId;
    private Object data;
}
