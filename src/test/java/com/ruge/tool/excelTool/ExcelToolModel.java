package com.ruge.tool.excelTool;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.*;

import java.util.Date;

/**
 * description
 * create Time at 2022/6/21 15:45
 *
 * @author alice.ruge
 * @since 0.0.7
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelToolModel {
    @ExcelProperty(value = "序号")
    private Integer id;
    @ExcelProperty(value = "姓名")
    private String name;

    @ExcelProperty(value = "性别")
    private Integer sex;

    @ExcelProperty(value = "生日")
    private Date birthday;
}
