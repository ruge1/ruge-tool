package com.ruge.tool.excelTool;

import com.ruge.tool.id.IdTool;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * description
 * create Time at 2022/6/21 15:43
 *
 * @author alice.ruge
 * @since 0.0.7
 */
@RestController
@RequestMapping("excel")
public class ExcelToolController {
    @SneakyThrows
    @GetMapping("download1")
    public void getAuthorizedPhoto(HttpServletResponse response) {
        List<ExcelToolModel> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(ExcelToolModel.builder().id(i).name("张" + i).build());
        }
        ExcelTool.download(response, ExcelToolModel.class, "sheet1", list, IdTool.randomUUID());
    }
}
