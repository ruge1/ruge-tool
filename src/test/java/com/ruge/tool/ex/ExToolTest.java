package com.ruge.tool.ex;

import org.junit.jupiter.api.Test;

class ExToolTest {

    @Test
    void isTure() {
        ExTool.isTure(true).throwMessage("如果参数为true抛出异常");
    }

    @Test
    void isTureOrFalse() {
        ExTool.isTureOrFalse(false).trueOrFalseHandle(() -> {
            System.out.println("true 开始");
        }, () -> {
            System.out.println("false 结束");
        });
    }

    @Test
    void isBlankOrNoBlank() {
        ExTool.isBlankOrNoBlank("0").presentOrElseHandle(System.out::println, () -> {
            System.out.println("空字符串");
        });
    }
}