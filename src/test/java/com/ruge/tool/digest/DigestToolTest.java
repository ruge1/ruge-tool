package com.ruge.tool.digest;


import org.junit.jupiter.api.Test;

public class DigestToolTest {

    @Test
    public void digest() {
        // MD5 加密，返回 32 位
        System.out.println(DigestTool.md5Hex("333"));
        // SHA-1 加密
        System.out.println(DigestTool.sha1Hex("333"));
        // SHA-256 加密
        System.out.println(DigestTool.sha256Hex("333"));
        // SHA-512 加密
        System.out.println(DigestTool.sha512Hex("333"));


    }

    @Test
    public void md5() {
    }

    @Test
    public void sha1() {
    }
}