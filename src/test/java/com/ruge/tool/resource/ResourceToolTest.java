package com.ruge.tool.resource;


import com.ruge.tool.resource.ResourceTool;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class ResourceToolTest {

    @Test
    public void getResourceAsSteam() {
        InputStream inputStream = ResourceTool.getResourceAsSteam("bootstrap.yml");

        String result = new BufferedReader(new InputStreamReader(inputStream))
                .lines().parallel().collect(Collectors.joining(System.lineSeparator()));
        System.out.println(result);
    }

    @Test
    public void getResourceAsString() {
        String result = ResourceTool.getResourceAsString("bootstrap.yml");
        System.out.println(result);
    }
}