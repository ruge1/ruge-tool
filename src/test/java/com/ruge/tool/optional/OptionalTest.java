package com.ruge.tool.optional;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

import java.util.Optional;

/**
 * description: https://mp.weixin.qq.com/s/Zch45LTxrgTBDqay5VwShg
 * create time at 2023/4/9 20:31
 *
 * @author alice.ruge
 * @since 0.0.3
 */

public class OptionalTest {

    @Test
    public void demo1() {
        System.out.println(getCityJdk7(new User(1, "张三", 18, new Address("长春市"))));
        System.out.println(getCityJdk7(new User(1, "张三", 18, null)));
    }

    @Test
    public void demo2() throws Exception {
        System.out.println(getCityJdk8(new User(1, "张三", 18, new Address("长春市"))));
        System.out.println(getCityJdk8(new User(1, "张三", 18, null)));
    }

    @Test
    public void demo3() {
        User user = new User(1, "张三", 18, new Address("长春市"));
        if (user != null) {
            System.out.println(user);
        }
        user = null;
        if (user != null) {
            System.out.println(user);
        }
    }

    @Test
    public void demo4() {
        User user = new User(1, "张三", 18, new Address("长春市"));
        Optional.ofNullable(user)
                .ifPresent(u -> {
                    System.out.println(u);
                });
        user = null;
        Optional.ofNullable(user)
                .ifPresent(u -> {
                    System.out.println(u);
                });
    }

    public String getCityJdk7(User user) {
        if (user != null) {
            if (user.getAddress() != null) {
                Address address = user.getAddress();
                if (address.getCity() != null) {
                    return address.getCity();
                }
            }
        }
        throw new RuntimeException("取值错误");
    }

    public String getCityJdk8(User user) throws Exception {
        return Optional.ofNullable(user)
                .map(User::getAddress)
                .map(Address::getCity)
                .orElseThrow(() -> new Exception("取指错误"));
    }

    public User getUserJkd7(User user) {
        if (user != null) {
            String name = user.getName();
            if ("zhangsan".equals(name)) {
                return user;
            } else {
                return null;
            }
        } else {
            user = new User();
            user.setName("zhangsan");
            return user;
        }
    }

    public User getUserJdk8(User user) {
        return Optional.ofNullable(user)
                .filter(u->"zhangsan".equals(u.getName()))
                .orElseGet(()-> {
                    User user1 = new User();
                    user1.setName("zhangsan");
                    return user1;
                });
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class User {
    private Integer id;
    private String name;
    private Integer age;
    private Address address;

}

@Data
@NoArgsConstructor
@AllArgsConstructor
class Address {
    private String city;
}