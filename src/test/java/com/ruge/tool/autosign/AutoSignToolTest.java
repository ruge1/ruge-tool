package com.ruge.tool.autosign;

import com.ruge.tool.json.JsonTool;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class AutoSignToolTest {

    public static void main(String[] args) {
        // 创建任务队列   10 为线程数量
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(10);
        // 初始化延迟
        long initialDelay = 0;
        // 固定的时间间隔
        long period = 1;
        scheduledExecutorService.scheduleAtFixedRate(AutoSignTool::autoSign, initialDelay, period, TimeUnit.HOURS);
    }
}