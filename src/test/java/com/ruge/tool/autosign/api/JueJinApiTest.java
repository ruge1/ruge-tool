package com.ruge.tool.autosign.api;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.ruge.tool.http.HttpExplorer;
import com.ruge.tool.http.HttpToolLaGou;
import com.ruge.tool.json.JsonTool;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class JueJinApiTest {

    @Test
    void getCountOfFreeDraw() {
        String header = "{\n" +
                "    \"accept\": \"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7\",\n" +
                "    \"accept-language\": \"zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6\",\n" +
                "    \"cache-control\": \"no-cache\",\n" +
                "    \"pragma\": \"no-cache\",\n" +
                "    \"priority\": \"u=0, i\",\n" +
                "    \"sec-ch-ua\": \"\\\"Not)A;Brand\\\";v=\\\"99\\\", \\\"Microsoft Edge\\\";v=\\\"127\\\", \\\"Chromium\\\";v=\\\"127\\\"\",\n" +
                "    \"sec-ch-ua-mobile\": \"?0\",\n" +
                "    \"sec-ch-ua-platform\": \"\\\"Windows\\\"\",\n" +
                "    \"sec-fetch-dest\": \"document\",\n" +
                "    \"sec-fetch-mode\": \"navigate\",\n" +
                "    \"sec-fetch-site\": \"none\",\n" +
                "    \"sec-fetch-user\": \"?1\",\n" +
                "    \"upgrade-insecure-requests\": \"1\"\n" +
                "  }";
        String url = "https://api.juejin.cn/growth_api/v1/lottery_config/get?aid=2608&uuid=7392842114574763557&spider=0&msToken=-JWCJSLzpDLjdi67w3cSQFnVeC5QVSkboSyqovOtxOjjvVkueiaH6D4xDagTivoigBabGKaj_GmiLe4yFKs3Tsu5TRzPQoVZW-hBtM6TgWJYGVVZsRlQ9DA0UaK1FXU%3D&a_bogus=xj-Evc2tMsm17DVf67Dz9twmxWE0YW-CgZENa9PUNzqX";
        HttpResponse response = HttpUtil.createGet(url)
                .addHeaders(JsonTool.getJsonToStringMap(header))
                .execute();
        System.out.println(url);
        System.out.println(JsonTool.getJsonToStringMap(header));
        System.out.println(response.body());
    }

    @Test
    void test2() {
        String header = "{\n" +
                "    \"accept\": \"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7\",\n" +
                "    \"accept-language\": \"zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6\",\n" +
                "    \"cache-control\": \"no-cache\",\n" +
                "    \"pragma\": \"no-cache\",\n" +
                "    \"priority\": \"u=0, i\",\n" +
                "    \"sec-ch-ua\": \"\\\"Not)A;Brand\\\";v=\\\"99\\\", \\\"Microsoft Edge\\\";v=\\\"127\\\", \\\"Chromium\\\";v=\\\"127\\\"\",\n" +
                "    \"sec-ch-ua-mobile\": \"?0\",\n" +
                "    \"sec-ch-ua-platform\": \"\\\"Windows\\\"\",\n" +
                "    \"sec-fetch-dest\": \"document\",\n" +
                "    \"sec-fetch-mode\": \"navigate\",\n" +
                "    \"sec-fetch-site\": \"none\",\n" +
                "    \"sec-fetch-user\": \"?1\",\n" +
                "    \"upgrade-insecure-requests\": \"1\"\n" +
                "  }";
        String url = "https://api.juejin.cn/growth_api/v1/lottery_config/get?aid=2608&uuid=7392842114574763557&spider=0&msToken=-JWCJSLzpDLjdi67w3cSQFnVeC5QVSkboSyqovOtxOjjvVkueiaH6D4xDagTivoigBabGKaj_GmiLe4yFKs3Tsu5TRzPQoVZW-hBtM6TgWJYGVVZsRlQ9DA0UaK1FXU%3D&a_bogus=xj-Evc2tMsm17DVf67Dz9twmxWE0YW-CgZENa9PUNzqX";
        HttpResponse response = HttpUtil.createGet(url)
                .addHeaders(JsonTool.getJsonToStringMap(header))
                .execute();
        System.out.println(url);
        System.out.println(JsonTool.getJsonToStringMap(header));
        System.out.println(response.body());
    }

    @Test
    void test3() {
        String header = "{\n" +
                "    \"accept\": \"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7\",\n" +
                "    \"accept-language\": \"zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6\",\n" +
                "    \"cache-control\": \"no-cache\",\n" +
                "    \"pragma\": \"no-cache\",\n" +
                "    \"priority\": \"u=0, i\",\n" +
                "    \"sec-ch-ua\": \"\\\"Not)A;Brand\\\";v=\\\"99\\\", \\\"Microsoft Edge\\\";v=\\\"127\\\", \\\"Chromium\\\";v=\\\"127\\\"\",\n" +
                "    \"sec-ch-ua-mobile\": \"?0\",\n" +
                "    \"sec-ch-ua-platform\": \"\\\"Windows\\\"\",\n" +
                "    \"sec-fetch-dest\": \"document\",\n" +
                "    \"sec-fetch-mode\": \"navigate\",\n" +
                "    \"sec-fetch-site\": \"none\",\n" +
                "    \"sec-fetch-user\": \"?1\",\n" +
                "    \"upgrade-insecure-requests\": \"1\"\n" +
                "  }";
        String url = "https://api.juejin.cn/growth_api/v1/lottery_config/get?aid=2608&uuid=7392842114574763557&spider=0&msToken=-JWCJSLzpDLjdi67w3cSQFnVeC5QVSkboSyqovOtxOjjvVkueiaH6D4xDagTivoigBabGKaj_GmiLe4yFKs3Tsu5TRzPQoVZW-hBtM6TgWJYGVVZsRlQ9DA0UaK1FXU%3D&a_bogus=xj-Evc2tMsm17DVf67Dz9twmxWE0YW-CgZENa9PUNzqX";
        HttpResponse response = HttpUtil.createGet(url)
                .addHeaders(JsonTool.getJsonToStringMap(header))
                .execute();
       // System.out.println(HttpExplorer.builder().setHeaderMap(JsonTool.getJsonToStringMap(header)).buildGet(url).toString());

        String result = HttpExplorer.builder().setHeaderMap(JsonTool.getJsonToStringMap(header)).build().get(url).getData().toString();

        System.out.println(url);
        System.out.println(JsonTool.getJsonToStringMap(header));
        System.out.println(response.body());
    }
}