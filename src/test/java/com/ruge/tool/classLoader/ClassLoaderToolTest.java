package com.ruge.tool.classLoader;

import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class ClassLoaderToolTest {

    @Test
    void getBootstrapURLs() {
        URL[] bootstrapURLs = ClassLoaderTool.getBootstrapURLs();
        for (URL bootstrapURL : bootstrapURLs) {
            System.out.println(bootstrapURL);
        }
    }

    @Test
    void getExtDirs() {
        String[] extDirs = ClassLoaderTool.getExtDirs();
        for (String extDir : extDirs) {
            System.out.println(extDir);
        }

    }
}