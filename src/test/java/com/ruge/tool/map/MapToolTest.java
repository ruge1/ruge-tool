package com.ruge.tool.map;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class MapToolTest {

    @Test
    public void newHashMap() {
        Map<String, Object> build = MapTool.newHashMap();
        build.put("333", "1231312");
    }


    @Test
    public void newHashMap2() {
        Map<String, Integer> stringIntegerMap = MapTool.newHashMap(3);
        Map<String, HashMap<String, String>> stringHashMapMap = MapTool.newHashMap(new HashMap<String, String>());

    }

    @Test
    public void newHashMap3() {
        Map<String, String> hashMap = MapTool.newHashMap("", "");
    }

    @Test
    public void newHashMap4() {
        String key = "";
        Integer value = 3;
        Map<String, Integer> hashMap = MapTool.newHashMap(key, value);
    }

    @Test
    public void getUrlParams() {
        String url = "https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=monline_7_dg&wd=%E6%B3%9B%E5%9E%8B%E5%8F%82%E6%95%B0%20%E6%96%B0%E5%BB%BAhashmap&oq=guava%2520%25E5%25A4%2584%25E7%2590%2586%2520map&rsv_pq=84cde76500891a75&rsv_t=25d1m5%2FA%2BAbL9jK3PqEJwLMuLEk%2FiWDFAJl0rsA9qIl0n3lHvb2fcbRgXvTOYIjg1sGQ&rqlang=cn&rsv_dl=tb&rsv_enter=1&rsv_sug3=30&rsv_sug1=9&rsv_sug7=100&rsv_sug2=0&rsv_btype=t&inputT=4784&rsv_sug4=8045";
        Map<String, String> map = MapTool.getUrlParams(url);
        map.forEach((k, v) -> System.out.println("key:" + k + "===value:" + v));
    }

    @Test
    public void buildUrlParams() {
        Map<String, String> map = MapTool.newHashMap("");
        map.put("a", "a");
        map.put("c", "c");
        map.put("b", "b");
        String urlParams = MapTool.buildUrlParams(map);
        System.out.println(urlParams);
    }

    @Test
    public void sortByKey() {
        Map<String, String> map = MapTool.newHashMap("");
        map.put("a", "a");
        map.put("c", "c");
        map.put("b", "b");
        Map<String, String> sort = MapTool.sortByKey(map);
        sort.forEach((k, v) -> {
            System.out.println("key:" + k + "===value:" + v);
        });
    }

    @Test
    public void sortByValue() {
        Map<String, String> map = MapTool.newHashMap("");
        map.put("a", "a");
        map.put("c", "c");
        map.put("b", "b");
        Map<String, String> sort = MapTool.sortByValue(map);
        sort.forEach((k, v) -> {
            System.out.println("key:" + k + "===value:" + v);
        });
    }
}