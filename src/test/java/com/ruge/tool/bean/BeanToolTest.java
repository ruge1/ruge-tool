package com.ruge.tool.bean;

import com.ruge.tool.lambda.LambdaTest;
import org.junit.jupiter.api.Test;

public class BeanToolTest {

    @Test
    public void set() {
        LambdaTest lambdaTest = new LambdaTest();
        lambdaTest.setId(666);
        BeanTool.set(lambdaTest, LambdaTest::getId, "1");
        System.out.println("默认有值不替换:" + lambdaTest);
        BeanTool.set(lambdaTest, LambdaTest::getId, "2", true);
        System.out.println("强制替换:" + lambdaTest);
        BeanTool.set(true, lambdaTest, LambdaTest::getId, "3");
        System.out.println("true 满足 condition 才替换:" + lambdaTest);
        BeanTool.set(false, lambdaTest, LambdaTest::getId, "4");
        System.out.println("false 满足 condition 才替换:" + lambdaTest);
    }
}