package com.ruge.tool.lambda;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class LambdaToolTest {

    @Test
    public void testConvertToFieldName() {
        System.out.println(LambdaTool.convertToFieldName(LambdaTest::getId));
        System.out.println(LambdaTool.convertToFieldName(LambdaTest::getName));
    }

    @Test
    public void forEachWithIndex() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        list.forEach(LambdaTool.forEachWithIndex((e, index) -> log.info("value:{},index:{}", e, index)));
    }

}