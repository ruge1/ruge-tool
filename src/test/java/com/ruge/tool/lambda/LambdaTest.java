package com.ruge.tool.lambda;

import lombok.Data;

/**
 * @author ruge.wu
 * @since 2021/12/24 16:32
 */
@Data
public class LambdaTest {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
