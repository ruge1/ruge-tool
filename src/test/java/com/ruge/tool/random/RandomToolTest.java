package com.ruge.tool.random;


import org.junit.jupiter.api.Test;

public class RandomToolTest {


    @Test
    public void testNextInt() {
        System.out.println(RandomTool.nextInt(1));
        System.out.println(RandomTool.nextInt());
        System.out.println(RandomTool.nextInt(1, 100));
    }

    @Test
    public void testNextLong() {
        System.out.println(RandomTool.nextLong(1));
        System.out.println(RandomTool.nextLong());
        System.out.println(RandomTool.nextLong(1, 100));
    }
}