package com.ruge.tool.math;

import org.junit.jupiter.api.Test;

class BigDecimalToolTest {


    @Test
    void defaultIfNull() {
        System.out.println(BigDecimalTool.zeroIfNull(null));
    }

    @Test
    void zeroIfNull() {
    }

    @Test
    void yuanToIntegerCent() {
    }

    @Test
    void yuanToCent() {
    }

    @Test
    void negate() {
    }

    @Test
    void defaultNegate() {
    }

    @Test
    void testNegate() {
    }

    @Test
    void testDefaultNegate() {
    }

    @Test
    void riseRate() {
    }

    @Test
    void addZeroIfNull() {
    }

    @Test
    void subtractZeroIfNull() {
    }

    @Test
    void notEqualZero() {
    }

    @Test
    void equalZero() {
    }

    @Test
    void notEqual() {
    }

    @Test
    void equal() {
    }

    @Test
    void testToString() {
    }

    @Test
    void toPlainString() {
    }

    @Test
    void toEngineeringString() {
    }

    @Test
    void getDecimalPlaces() {
    }

    @Test
    void isEqual() {
    }

    @Test
    void isInteger() {
    }
}