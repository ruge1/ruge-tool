package com.ruge.tool.id;

import org.junit.jupiter.api.Test;


class IdToolTest {
    @Test
    public void genOrderId() {
        String prefix = "QSC";
        String qsc = IdTool.genOrderId(prefix, null);
        System.out.println(qsc);
        qsc = IdTool.genOrderId(prefix, qsc);
        System.out.println(qsc);
    }

}