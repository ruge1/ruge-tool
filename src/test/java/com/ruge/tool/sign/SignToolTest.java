package com.ruge.tool.sign;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ruge.tool.date.DateTool;
import com.ruge.tool.http.HttpExplorer;
import com.ruge.tool.json.JsonTool;
import com.ruge.tool.map.MapTool;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.UUID;

@Slf4j
public class SignToolTest {


    @SneakyThrows
    @Test
    public void build_PROD_31() {
        // 大五座揽巡 数据详情
        String envUrl = "https://mg-uat.arcfox.cn/";
        String serverUrl = "mall-integral/internal/integral/syncIntegral?syncType=28&aid=261630";
        String appkey = "2368392532";
        String secretKey = "26abfc143bfe3cda422d91e4c9ca4783";

        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);

        String body = HttpUtil.createGet(buildTimaUrl).execute().body();
        log.info("body:{}", body);
    }

    @SneakyThrows
    @Test
    public void buildTimaUrl2() {
        String envUrl = "https://mg-uat.arcfox.cn/";
        String serverUrl = "vehicle-customer/public/vehicle/getDefaultVehicle?signt=1683720272451&sign=2562c31526cf3842644f25b610a07f90&appkey=2368392532&nonce=25fc1a0c-d5a8-4f75-a513-1a320a2f1b25&token=ckRuSHNUZmVidmxEUHlwYTU3VnB5UWRpT2NzcmV6NnNEOC9zZzFZSVhQcHNxYlR2aWpwMDZ1WHB2Q01WOGhIbTZyMkFzdVE0NE5LM3RFUWhnVExTZHc9PQ--___1683166496680___224986___2368392532___MOBILE";
        String appkey = "2368392532";
        String secretKey = "26abfc143bfe3cda422d91e4c9ca4783";
        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        System.out.println(buildTimaUrl);
//        String result = HttpTool.get(buildTimaUrl);
    }

    @SneakyThrows
    @Test
    public void buildTimaUrl() {
        String envUrl = "https://bg-uat.arcfox.cn/";
        String serverUrl = "vehicle-after-sales/internal/maintenancev2/getUserByVinAndMobile?vin=LNBMCB4K6MZ012089&mobile=18808929786";
        String appkey = "2368392532";
        String secretKey = "26abfc143bfe3cda422d91e4c9ca4783";
        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        System.out.println(buildTimaUrl);
//        String result = HttpTool.get(buildTimaUrl);
    }

    @SneakyThrows
    @Test
    public void buildAvrisUat() {
        String envUrl = "https://operations-center-web-uat.faw-vw.com/";
        String serverUrl = "operation-interface/queryPublish/queryList?areaCode=RSD6%3B510100%3B510112&contactId=4&showPositionId=9&aid=17062179&requestId=5d1f72112ac5461b9e185ad43bec34c6&clientId=945112220119135997&timestamp=" + System.currentTimeMillis() + "&nonce=5d1f72&sign=b8f40553ce7c74fd5665c9559a1b96fd";
        String appkey = "cd-faw-test";
        String secretKey = "123456";
        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        String result = HttpExplorer.builder().build().get(buildTimaUrl).getData().toString();
        System.out.println(result);
    }

    @SneakyThrows
    @Test
    public void buildAvrisProd_operations() {

        long signt = System.currentTimeMillis();
        String envUrl = "https://one-app-admin-uat.faw-vw.com/uat2-api/web/";
        Map<String, String> map = MapTool.newHashMap("");
        map.put("appkey", "cd-faw-test");
        map.put("nonce", UUID.randomUUID().toString());
        map.put("signt", "" + signt);
        log.error("时间戳参数:{}", signt);
        log.error("北京时间 开始:{}", DateTool.formatDateTime(signt));

        String serverUrl = "integration/operation/get_token?" + MapTool.buildUrlParams(map);
        String appkey = "cd-faw-test";
        String secretKey = "123456";
        Map<String, String> headers = MapTool.newHashMap("");
//        headers.put("Authorization", "admin@A@1667184951379");
        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("account", "admin");
        jsonBody.put("authKey", "1667781836650");
        String bodyString = jsonBody.toString();
        log.info("url:{}", buildTimaUrl);
        log.info("bodyString:{}", bodyString);
        log.info("headers:{}", headers);
        String body = HttpUtil.createPost(buildTimaUrl).body(bodyString).headerMap(headers, true).execute().body();
        log.info("body:{}", body);
        log.error("北京时间 结束:{}", DateTool.formatDateTime(signt));
    }


    @SneakyThrows
    @Test
    public void build_UAT_operations() {

        String envUrl = "https://one-app-admin-uat.faw-vw.com/uat2-api/web/";
        String serverUrl = "integration/operation/get_token";
        String appkey = "cd-faw-test";
        String secretKey = "123456";

        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        Map<String, String> headers = MapTool.newHashMap("");
        Map<String, String> bodys = MapTool.newHashMap("");
        bodys.put("account", "admin");
        bodys.put("authKey", "1667781836650");

        String body = HttpUtil.createPost(buildTimaUrl).body(JsonTool.getObjToJson(bodys)).headerMap(headers, true).execute().body();
        log.info("body:{}", body);
    }


    @SneakyThrows
    @Test
    public void build_PROD_operations() {

        String envUrl = "https://operations-center-web-uat.faw-vw.com/";
        String serverUrl = "operation-interface/queryPublish/queryList?pageSize=100&pageNo=1&areaCode=+%3B+%3B+&contactId=30&showPositionId=175&aid=12&requestId=77c841a931d340b5a3870bdd4fe022ab&clientId=638991220110135798&timestamp=1667979749570&nonce=77c841&sign=a15d07a5cd7e03945e36b505e422ce0";
        String appkey = "cd-faw-test";
        String secretKey = "123456";

        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);
        Map<String, String> headers = MapTool.newHashMap("");
        Map<String, String> bodys = MapTool.newHashMap("");
        bodys.put("account", "admin");
        bodys.put("authKey", "1667781836650");

        String body = HttpUtil.createPost(buildTimaUrl).body(JsonTool.getObjToJson(bodys)).headerMap(headers, true).execute().body();
        log.info("body:{}", body);
    }

    @SneakyThrows
    @Test
    public void build_PROD_2() {
        // 大五座揽巡 数据详情
        String envUrl = "https://operations-center-web.faw-vw.com/cdp/external/";
        String serverUrl = "marketing-adapter/oc/dashboard/v1/query-content-display?id=27294&contentId=1000401&startDate=2022-11-08&endDate=2022-11-14";
        String appkey = "be7311d7578c4aa39989c7651ce52e47";
        String secretKey = "95822AF4F2DBEBCA1F0E230D5DD5048E";

        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);

        String body = HttpUtil.createGet(buildTimaUrl).execute().body();
        log.info("body:{}", body);
    }

    @SneakyThrows
    @Test
    public void build_PROD_3() {
        // 大五座揽巡 数据详情
        String envUrl = "http://192.168.175.34/";
        String serverUrl = "one-app/general/manage/v1/integration/operation/data_active?id=27651";
        String appkey = "be7311d7578c4aa39989c7651ce52e47";
        String secretKey = "95822AF4F2DBEBCA1F0E230D5DD5048E";

        String buildTimaUrl = SignTool.buildTimaUrl(envUrl, serverUrl, appkey, secretKey);

        log.info("url:{}", buildTimaUrl);
        String body = HttpUtil.createGet(buildTimaUrl).execute().body();
        log.info("body:{}", body);
    }

}