package com.ruge.tool.system;

import com.ruge.tool.system.SystemTool;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
class SystemToolTest {

    @Test
    void lineSeparator() {
        log.info("张三第一:{},李四第二:{},王五第三:{}", SystemTool.lineSeparator(), SystemTool.lineSeparator(), SystemTool.lineSeparator());

    }
}