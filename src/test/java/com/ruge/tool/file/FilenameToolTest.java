package com.ruge.tool.file;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


class FilenameToolTest {
    @Test
    public void test() {
        String path = "E:\\work\\a.jpg";
        System.out.println("获取文件完整路径，不含文件名: " + FilenameTool.getFullPath(path));
        System.out.println("获取文件不含结尾分隔符的完整路径，不含文件名: " + FilenameTool.getFullPathNoEndSeparator(path));
        System.out.println("获取不含前缀和结尾分隔符的完整文件路径，不含文件名: " + FilenameTool.getPathNoEndSeparator(path));
        System.out.println("获取不含前缀的完整文件路径，不含文件名: " + FilenameTool.getPath(path));
        System.out.println("获取文件路径前缀: " + FilenameTool.getPrefix(path));
        System.out.println("获取文件扩展名: " + FilenameTool.getExtension(path));
        System.out.println("获取文件基本名: " + FilenameTool.getBaseName(path));
        System.out.println("获取单独的文件名及其后缀: " + FilenameTool.getName(path));
        List<String> suffixList = new ArrayList<>();
        suffixList.add("png");
        suffixList.add("txt");
        suffixList.add("xlsx");
        suffixList.add("jpg");
        System.out.println("判断文件扩展名是否符合给定的:" + FilenameTool.isExtension(path, suffixList));
        System.out.println("判断文件扩展名是否符合给定的:" + FilenameTool.isExtension(path, "txt"));
        System.out.println("转换文件分割为系统分隔符:" + FilenameTool.separatorsToSystem(path));
        System.out.println("转换文件分割为unix系统分隔符:" + FilenameTool.separatorsToUnix(path));
        System.out.println("转换文件分割为windows系统分隔符:" + FilenameTool.separatorsToWindows(path));
        System.out.println("系统分隔符:" + FilenameTool.SYSTEM_SEPARATOR);
    }
}