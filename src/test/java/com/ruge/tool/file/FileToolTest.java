package com.ruge.tool.file;


import com.ruge.tool.compress.CompressTool;
import com.ruge.tool.json.JsonTool;
import com.ruge.tool.map.MapTool;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FileToolTest {

    @SneakyThrows
    @Test
    public void getTempDirectoryPath() {
        File file = new File("E:\\work\\a.jpg");
        //将文件的字节大小转换成我们显示的字符串，比如1024转换后1kb
        String displaySize = FileTool.byteCountToDisplaySize(file.length());
        System.out.println(displaySize);
        // 清空文件夹中的所有文件和子文件夹，文件夹本身不会被清除
        FileTool.cleanDirectory(new File("E:\\tmp"));
        // 文件清理 多个重载方法
//        FileTool.deleteDirectory();
        // 文件拷贝  多个重载方法
//        FileTool.copyDirectory();
        // 文件夹是否包含某个文件
        boolean directoryContains = FileTool.directoryContains(new File("E:\\work"), new File("E:\\work\\a.jpg"));
        System.out.println(directoryContains);
        // 强制删除
//        FileTool.forceDelete();
        // 获取系统的临时目录路径
        String tempDirectoryPath = FileTool.getTempDirectoryPath();
        System.out.println(tempDirectoryPath);
        // 获取用户的主目录
        String userDirectoryPath = FileTool.getUserDirectoryPath();
        System.out.println(userDirectoryPath);
        //返回文件迭代器  [ 目录 ] [ 过滤器 ] [ 递归 ]
        Iterator<File> files = FileTool.iterateFiles(new File("E:\\data"), null, true);
        files.forEachRemaining(System.out::println);
        // 列出该目录下的所有doc文件，递归（扩展名不必带.doc）
        FileTool.listFiles(new File("E:\\work"), new String[]{"doc"}, true);
        //文件移动  多个重载方法
//        FileTool.moveDirectory(, );
        // 文件转流
//        FileTool.openInputStream();
        // 文件读取
        List<String> strings = FileTool.readLines(new File("E:\\work\\postman.json"), Charset.defaultCharset());
        strings.forEach(System.out::println);
        // 文件大小
        System.out.println(FileTool.sizeOf(new File("E:\\work\\postman.json")));
        // URL To File
//        FileTool.toFile();
        // 文件写入 多个方法重载
//        FileTool.write();
    }

    @SneakyThrows
    @Test
    public void listFile2() {

    }

    public void removeUrl(List<File> result) {
        for (File file : result) {
            if (file.toString().endsWith(".url")) {
                file.delete();
            }
        }
    }

    public void rename(List<File> result) {
        for (File e : result) {
            FileTool.rename(e, e.getName().replace(" ", "_"));
            FileTool.rename(e, e.getName().replace("【海 量资源：666java.com】", ""));
            FileTool.rename(e, e.getName().replace("【www.666java.com4】", ""));
            FileTool.rename(e, e.getName().replace("【www.666java.com】", ""));
            FileTool.rename(e, e.getName().replace("【海 量资源：666java.com】", ""));
            FileTool.rename(e, e.getName().replace("【海量资源：666java.com】", ""));
            FileTool.rename(e, e.getName().replace("【www.666java.com】【海量资源：666java.com】", ""));
            FileTool.rename(e, e.getName().replace("【海量资源：666java.com4】", ""));
            FileTool.rename(e, e.getName().replace("【海量资源：666java.com】", ""));
        }
        String keyword = "_";
        result = result.stream().filter(e -> e.getName().endsWith(".mp4")).collect(Collectors.toList());
        for (int i = 0; i < result.size(); i++) {
            File file = result.get(i);
            //得到一个NumberFormat的实例
            NumberFormat nf = NumberFormat.getInstance();
            //设置是否使用分组
            nf.setGroupingUsed(false);
            //设置最大整数位数
            nf.setMaximumIntegerDigits(3);
            //设置最小整数位数
            nf.setMinimumIntegerDigits(3);
            String newFileNamePrefix = nf.format(i) + "_";
            newFileNamePrefix = "_";
            String oldFileName = file.getName();
            String newFileName = newFileNamePrefix + file.getName().substring(oldFileName.indexOf(keyword) + 1);
//            FileTool.rename(file, newFileName);
        }
    }

    @Test
    public void listFile() {
        String path =  "E:\\BaiduNetdiskDownload\\Java项目_品达通用权限系统(高含金量 一套通所有)\\资料";
        List<File> result = new ArrayList<>();
        FileTool.listFile(new File(path), result);
         // 移除.url结尾的文件
        removeUrl(result);

        List<File> collect = result.stream().filter(e ->
                e.getName().endsWith(".rar")
                        || e.getName().endsWith(".zip")
                        || e.getName().endsWith(".7z")
                        || e.getName().endsWith(".war")
        )
                .collect(Collectors.toList());
        System.out.println("文件个数:"+result.size());
        collect.forEach(System.out::println);
//        collect.forEach(e -> CompressTool.unCompress(e.getAbsolutePath(), "www.itjc8.com@o2M*b$eO@CPH"));
        // 文件重命名
//        rename(result);

    }

    @Test
    public  void replaceKeyWord(){
        Map<String, String> map = JsonTool.getJsonToStringMap("{\n" +
                "    \"Mini0@dmin\": \"ENC(+gq16Lni99wGwo2MW8sefCO4wPEBBZjJ)\",\n" +
                "    \"271dc024f99bd928eff7b3a3c572adf0\": \"ENC(xveFiPfyus1jmgNlEGzvTpMyBlc0+d/jgottOB4fQipqRuU0i5WCBLLeONd96H11)\",\n" +
                "    \"W7h1gFPXgD4PBOL8p98oKG3Lu60NTz4u\": \"ENC(v914N8oR+1AKjxWYJIZNJ6WB/ahNq+lwg4pdsyXY1614fSA7EhtvJ9Ds1qXDADBF)\",\n" +
                "    \"d082jALIzQbWBblgMnzvc4FAoCJAk59x\": \"ENC(s8liqb3lNhnXaQiMKBTl8yCDs53XFBXwPHXR/ZuTy2XMioTRCi40PIdh4Lkv5D6J)\",\n" +
                "    \"8c83dd307f1beb0057c0fc1c7fcb22b8\": \"ENC(wmGyIhrBbFnBxa0gBQi6Aj31Z4b1ZJVtGPD1E5bevdAY3W0EXeKsbWb6kDSgrmDK)\",\n" +
                "    \"95822AF4F2DBEBCA1F0E230D5DD5048E\": \"ENC(h2L4b9RnwsybqwrIBS5Dy17GhRs847Lb2CgheccYq4Dwa+296HjvQGOlT9ST3Avh)\",\n" +
                "    \"ZhIaSLO7rDsNo6UVA8KY\": \"ENC(b5rMwQ4PcKVofwOo4MCIcua6nr2cPDQRbobpWP6j+CA=)\",\n" +
                "    \"4qUExfwzlc0YOjpt3XM6\": \"ENC(RXZCcRaF0kr7jLmxBFsRXb2KKYtrJRJwlKD+HrfK1do=)\"\n" +
                "  }");
        FileTool.replaceKeyWord(new File("E:\\data\\avris\\DEFAULT_GROUP\\avris-operation-report-biz-dev.yml"), map);
    }
}
