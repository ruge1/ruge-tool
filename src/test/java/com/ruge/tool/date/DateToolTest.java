package com.ruge.tool.date;

import com.ruge.tool.enums.DateEnum;
import com.ruge.tool.enums.DateFormatEnum;
import com.ruge.tool.enums.OrderEnum;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DateToolTest {

    @Test
    public void getDate() {
        System.out.println(DateTool.getDate());
        System.out.println(DateTool.getDate(System.currentTimeMillis()));
        System.out.println(DateTool.getDate(LocalDateTime.now()));
        System.out.println(DateTool.getDate(new Date()));
    }

    @Test
    public void getLocalDateTime() {
        System.out.println(DateTool.getLocalDateTime());
        System.out.println(DateTool.getLocalDateTime(System.currentTimeMillis()));
        System.out.println(DateTool.getLocalDateTime(LocalDateTime.now()));
        System.out.println(DateTool.getLocalDateTime(new Date()));
    }

    @Test
    public void getTimestamp() {
        System.out.println(DateTool.getTimestamp());
        System.out.println(DateTool.getTimestamp(System.currentTimeMillis()));
        System.out.println(DateTool.getTimestamp(LocalDateTime.now()));
        System.out.println(DateTool.getTimestamp(new Date()));
    }

    @Test
    public void addYear() {
        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addYear(1));
        System.out.println(DateTool.addYear(-1));

        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addYear(DateTool.getDate(), 1));
        System.out.println(DateTool.addYear(DateTool.getDate(), -1));


        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addYear(DateTool.getDate().getTime(), 1));
        System.out.println(DateTool.addYear(DateTool.getDate().getTime(), -1));
    }

    @Test
    public void addMonth() {
        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addMonth(1));
        System.out.println(DateTool.addMonth(-1));

        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addMonth(DateTool.getDate(), 1));
        System.out.println(DateTool.addMonth(DateTool.getDate(), -1));


        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addMonth(DateTool.getDate().getTime(), 1));
        System.out.println(DateTool.addMonth(DateTool.getDate().getTime(), -1));
    }


    @Test
    public void addDay() {
        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addDay(1));
        System.out.println(DateTool.addDay(-1));

        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addDay(DateTool.getDate(), 1));
        System.out.println(DateTool.addDay(DateTool.getDate(), -1));


        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addDay(DateTool.getDate().getTime(), 1));
        System.out.println(DateTool.addDay(DateTool.getDate().getTime(), -1));
    }


    @Test
    public void addHour() {
        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addHour(1));
        System.out.println(DateTool.addHour(-1));

        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addHour(DateTool.getDate(), 1));
        System.out.println(DateTool.addHour(DateTool.getDate(), -1));


        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addHour(DateTool.getDate().getTime(), 1));
        System.out.println(DateTool.addHour(DateTool.getDate().getTime(), -1));
    }


    @Test
    public void addMinute() {
        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addMinute(1));
        System.out.println(DateTool.addMinute(-1));

        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addMinute(DateTool.getDate(), 1));
        System.out.println(DateTool.addMinute(DateTool.getDate(), -1));


        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addMinute(DateTool.getDate().getTime(), 1));
        System.out.println(DateTool.addMinute(DateTool.getDate().getTime(), -1));
    }

    @Test
    public void addSecond() {
        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addSecond(1));
        System.out.println(DateTool.addSecond(-1));

        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addSecond(DateTool.getDate(), 1));
        System.out.println(DateTool.addSecond(DateTool.getDate(), -1));


        System.out.println("原始日期:" + DateTool.getDate());
        System.out.println(DateTool.addSecond(DateTool.getDate().getTime(), 1));
        System.out.println(DateTool.addSecond(DateTool.getDate().getTime(), -1));
    }


    @Test
    public void getBegin() {
        System.out.println("当前时间:" + DateTool.getDate());
        System.out.println("默认日初:" + DateTool.getDayBegin());
        System.out.println("默认日末:" + DateTool.getDayEnd());
        System.out.println("默认月初:" + DateTool.getMonthBegin());
        System.out.println("默认月末:" + DateTool.getMonthEnd());


        System.out.println("日初:" + DateTool.getDayBegin(DateTool.getDate()));
        System.out.println("日初:" + DateTool.getDayBegin(DateTool.getTimestamp()));
        System.out.println("日末:" + DateTool.getDayEnd(DateTool.getDate()));
        System.out.println("日末:" + DateTool.getDayEnd(DateTool.getTimestamp()));

        System.out.println("月初:" + DateTool.getMonthBegin(DateTool.getDate()));
        System.out.println("月初:" + DateTool.getMonthBegin(DateTool.getTimestamp()));
        System.out.println("月末:" + DateTool.getMonthEnd(DateTool.getDate()));
        System.out.println("月末:" + DateTool.getMonthEnd(DateTool.getTimestamp()));
    }


    @Test
    public void getBetween() {
        Date startTime = DateTool.addYear(-1);
        Date endTime = DateTool.addYear(1);
        Object startStopTime = DateTool.getMonthBegin();
        Object endStopTime = DateTool.addYear(0);
        List<String> authDate = DateTool.getBetween(startTime, endTime, startStopTime, endStopTime, DateEnum.DAY, OrderEnum.ASC);
        authDate.forEach(System.out::println);
        System.out.println("===========================================");
        DateTool.getBetween(startTime, endTime).forEach(System.out::println);
    }


    @Test
    public void getDayByHour() {
        Date startTime = DateTool.getDate();
        System.out.println(DateTool.getDayByHour(startTime, 4));

    }

    @Test
    public void format() {
        System.out.println(DateTool.format(null, DateFormatEnum.DATE_MEDIUM, ""));
        System.out.println(DateTool.format(null, DateFormatEnum.TIME_MEDIUM, ""));
        System.out.println(DateTool.format(null, DateFormatEnum.DATE_TIME_MEDIUM, ""));
        System.out.println(DateTool.formatDate());
        System.out.println(DateTool.format(new Date()));
        System.out.println(DateTool.format("yyyy-MM-dd"));
        System.out.println(DateTool.format(new Date(), "yyyy-MM-dd HH"));
        System.out.println(DateTool.formatDateTime());
    }

    @Test
    public void getDefaultDate() {

        // 获取不同格式化风格和中国环境的日期
        DateFormat df1 = DateFormat.getDateInstance(DateFormat.SHORT, Locale.CHINA);
        DateFormat df2 = DateFormat.getDateInstance(DateFormat.FULL, Locale.CHINA);
        DateFormat df3 = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat df4 = DateFormat.getDateInstance(DateFormat.LONG, Locale.CHINA);
        // 获取不同格式化风格和中国环境的时间
        DateFormat df5 = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.CHINA);
        DateFormat df6 = DateFormat.getTimeInstance(DateFormat.FULL, Locale.CHINA);
        DateFormat df7 = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat df8 = DateFormat.getTimeInstance(DateFormat.LONG, Locale.CHINA);
        // 将不同格式化风格的日期格式化为日期字符串
        String date1 = df1.format(new Date());
        String date2 = df2.format(new Date());
        String date3 = df3.format(new Date());
        String date4 = df4.format(new Date());
        // 将不同格式化风格的时间格式化为时间字符串
        String time1 = df5.format(new Date());
        String time2 = df6.format(new Date());
        String time3 = df7.format(new Date());
        String time4 = df8.format(new Date());
// 输出日期
        System.out.println("SHORT：" + date1 + "===" + time1);
        System.out.println("FULL：" + date2 + "===" + time2);
        System.out.println("MEDIUM：" + date3 + "===" + time3);
        System.out.println("LONG：" + date4 + "===" + time4);
    }

    @Test
    public void parseDate() {
        System.out.println(DateTool.formatDateTime(DateTool.parseDate("2022-01-01")));
        System.out.println(DateTool.formatDateTime(DateTool.parseDate("2022-01-01 11:11:11","yyyy-MM-dd HH:mm:ss")));
        System.out.println(DateTool.formatDateTime(DateTool.parseDate("2022-01-01 11:11:11",DateFormatEnum.DATE_TIME_MEDIUM)));
        System.out.println(DateTool.formatDateTime(DateTool.parseDate("2022-01-01 11:11:11",DateFormatEnum.DATE_TIME_MEDIUM,"yyyy-MM-dd hh:MM:ss")));
        System.out.println(DateTool.formatDateTime(DateTool.parseDate("2022-01-01 11:11:11",DateFormatEnum.DATE_TIME_MEDIUM,null)));
    }



}