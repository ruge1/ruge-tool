package com.ruge.tool.str;


import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class StringToolTest {

    @Test
    public void concat() {
        System.out.println(StringTool.concat("我", "爱", "中国"));
    }


    @Test
    public void format() {
    }

    @Test
    public void builder() {
    }

    @Test
    public void isBlank() {
        Map<String, Object> map = new HashMap<>();
        map.put("1", "");
        System.out.println(StringTool.isBlank(map));
        System.out.println(StringTool.isBlank(Collections.singletonList("")));
    }

    @Test
    public void testIsBlank() {
    }

    @Test
    public void isNotBlank() {
    }

    @Test
    public void testIsNotBlank() {
    }

    @Test
    public void isAllLowerCase() {
    }

    @Test
    public void isAllUpperCase() {
    }

    @Test
    public void upperTable() {
    }

    @Test
    public void mustStartWith() {
        System.out.println(StringTool.mustStartWith("vehicle-after-sales/internal/maintenancev2/getUserByVinAndMobile?vin=LNBMCB4K6MZ012089&mobile=18808929786", "/"));
    }

    @Test
    public void mustNotStartWith() {
        System.out.println(StringTool.mustNotStartWith("/vehicle-after-sales/internal/maintenancev2/getUserByVinAndMobile?vin=LNBMCB4K6MZ012089&mobile=18808929786", "/"));

    }

    @Test
    public void mustEndWith() {
        System.out.println(StringTool.mustEndWith("https://bg-uat.arcfox.cn", "/"));
        System.out.println(StringTool.mustEndWith("https://bg-uat.arcfox.cn/", "/"));
    }

    @Test
    public void mustNotEndWith() {
        System.out.println(StringTool.mustNotEndWith("https://bg-uat.arcfox.cn", "/"));
        System.out.println(StringTool.mustNotEndWith("https://bg-uat.arcfox.cn/", "/"));

    }

    @Test
    public void getRequestURI() {
        System.out.println(StringTool.getRequestUrl("https://blog.csdn.net/Bao_S_J/article/details/62056543"));
    }

    @Test
    public void upperCaseFirst() {
    }

    @Test
    public void lowerCaseFirst() {
    }

    @Test
    public void toLowerCaseFirstOne() {
    }

    @Test
    public void toUpperCaseFirstOne() {
    }

    @Test
    public void strToInteger() {
    }

    @Test
    public void replaceFirst() {
        String s = StringTool.replaceFirst("getId", "s");
        System.out.println(s);
    }

    @Test
    public void getRequestParams() {
    }

    @Test
    public void getGameNickName() {
        System.out.println(StringTool.getGameNickName(4));
    }
}