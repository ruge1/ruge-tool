package com.ruge.tool.tree;

import com.ruge.tool.list.TreeNodeDTO;
import lombok.*;

/**
 * @author fubo
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DemoTreeNode extends TreeNodeDTO {
    /**
     * 节点字段1
     */
    private String column1;
    /**
     * 节点字段2
     */
    private String column2;

}