package com.ruge.tool.tree;

import com.ruge.tool.json.JsonTool;
import com.ruge.tool.list.TreeTool;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class TreeToolTest {

    public static List<DemoTreeNode> list2 = new ArrayList<>();

    @BeforeEach
    void before() {
        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(1L);
            demoTreeNode1.setParentId(0L);
            demoTreeNode1.setColumn1("中国");
            list2.add(demoTreeNode1);
        }
        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(2L);
            demoTreeNode1.setParentId(0L);
            demoTreeNode1.setColumn1("美国");
            list2.add(demoTreeNode1);
        }

        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(3L);
            demoTreeNode1.setParentId(1L);
            demoTreeNode1.setColumn1("河北");
            list2.add(demoTreeNode1);
        }

        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(4L);
            demoTreeNode1.setParentId(1L);
            demoTreeNode1.setColumn1("上海");
            list2.add(demoTreeNode1);
        }

        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(5L);
            demoTreeNode1.setParentId(3L);
            demoTreeNode1.setColumn1("石家庄");
            list2.add(demoTreeNode1);
        }

        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(6L);
            demoTreeNode1.setParentId(3L);
            demoTreeNode1.setColumn1("张家口");
            list2.add(demoTreeNode1);
        }

        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(7L);
            demoTreeNode1.setParentId(5L);
            demoTreeNode1.setColumn1("赵县");
            list2.add(demoTreeNode1);
        }
        if (true) {
            DemoTreeNode demoTreeNode1 = new DemoTreeNode();
            demoTreeNode1.setId(8L);
            demoTreeNode1.setParentId(5L);
            demoTreeNode1.setColumn1("长安区");
            list2.add(demoTreeNode1);
        }
    }


    @Test
    void testListToTree() {
        List<DemoTreeNode> list = TreeTool.listToTree(list2, 1L);
        list.forEach(e -> System.out.println(e.getChildrenList().size()));
        System.out.println(JsonTool.getObjToJson(list));
    }
}