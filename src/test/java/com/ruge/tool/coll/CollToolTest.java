package com.ruge.tool.coll;

import org.junit.jupiter.api.Test;

import java.util.*;

class CollToolTest {

    @Test
    void reverse() {
    }

    @Test
    void shuffle() {
    }

    @Test
    void testShuffle() {
    }

    @Test
    void iteratorToList() {
    }

    @Test
    void sortByKey() {
    }

    @Test
    void list() {
        String[] strArray = {"aaa", "bbb", "ccc", "aaa"};
        List strList = new ArrayList();
        Set<Integer> strSet = new HashSet<>();
        strSet.add(1);
        strSet.add(2);
        strSet.add(3);
        System.out.println("=======================arr2list======================");
        List<String> strings = CollTool.toList(strArray);
        strings.forEach(System.out::println);

        System.out.println("=======================arr2set======================");
        Set<String> strings1 = CollTool.toSet(strArray);
        strings1.forEach(System.out::println);
        System.out.println("=======================set2list======================");
        List<Integer> toList = CollTool.toList(strSet);
        toList.forEach(System.out::println);
        System.out.println("=======================set2list======================");
        Set<Integer> set = CollTool.toSet(toList);
        set.forEach(System.out::println);
    }

    @Test
    void conditionAdd() {
        ArrayList<Object> list = new ArrayList<>();
        CollTool.conditionAdd(true, list, "张三1");
        CollTool.conditionAdd(false, list, "张三2");
        list.forEach(System.out::println);

        HashSet<Object> set = new HashSet<>();
        CollTool.conditionAdd(true, set, " 李四1");
        CollTool.conditionAdd(false, set, "李四2");
        set.forEach(System.out::println);
    }

    @Test
    void conditionBlackAdd() {
        ArrayList<Object> list = new ArrayList<>();
        CollTool.conditionBlackAdd(true, list, "张三1");
        CollTool.conditionBlackAdd(false, list, "张三2");
        CollTool.conditionBlackAdd(null, list, "张三3");
        CollTool.conditionBlackAdd("", list, "张三4");
        CollTool.conditionBlackAdd(3L, list, "张三5");
        list.forEach(System.out::println);

        HashSet<Object> set = new HashSet<>();
        CollTool.conditionBlackAdd(true, set, " 李四1");
        CollTool.conditionBlackAdd(false, set, "李四2");
        CollTool.conditionBlackAdd(null, set, "李四3");
        CollTool.conditionBlackAdd("", set, "李四4");
        CollTool.conditionBlackAdd(3L, set, "李四5");
        set.forEach(System.out::println);
    }

    @Test
    void conditionNotBlackAdd() {
        ArrayList<Object> list = new ArrayList<>();
        CollTool.conditionNotBlackAdd(true, list, "张三1");
        CollTool.conditionNotBlackAdd(false, list, "张三2");
        CollTool.conditionNotBlackAdd(null, list, "张三3");
        CollTool.conditionNotBlackAdd("", list, "张三4");
        CollTool.conditionNotBlackAdd(3L, list, "张三5");
        list.forEach(System.out::println);

        HashSet<Object> set = new HashSet<>();
        CollTool.conditionNotBlackAdd(true, set, " 李四1");
        CollTool.conditionNotBlackAdd(false, set, "李四2");
        CollTool.conditionNotBlackAdd(null, set, "李四3");
        CollTool.conditionNotBlackAdd("", set, "李四4");
        CollTool.conditionNotBlackAdd(3L, set, "李四5");
        set.forEach(System.out::println);
    }
}