package com.ruge.tool.baidu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BaiDuApiTest {

    @Test
    void getAuth() {
        String token = BaiDuApi.getAuth("727OG5d5bmeKwSuTlvbyO2Gz", "GGWbMz9mABiKsFTPcdVsYjNOxevmlPgd ");
        System.out.println(token);
    }

    @Test
    void audioFileTransfer() {
        String token = BaiDuApi.getAuth("727OG5d5bmeKwSuTlvbyO2Gz", "GGWbMz9mABiKsFTPcdVsYjNOxevmlPgd ");
        String fileTransfer = BaiDuApi.AudioFileTransfer(token, "https://ruge-audio-file.bj.bcebos.com/%E4%B8%9C%E5%8D%97%E6%B9%96%E5%A4%A7%E8%B7%AF%202.m4a");
        System.out.println(fileTransfer);
    }
}