package com.ruge.tool.json;

import com.ruge.tool.date.DateTool;
import com.ruge.tool.enums.DateFormatEnum;
import lombok.*;
import org.junit.jupiter.api.Test;

import java.util.*;

public class JsonToolTest {

    public static final String json = "{\n" +
            "    \"accept\": \"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7\",\n" +
            "    \"accept-language\": \"zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6\",\n" +
            "    \"cache-control\": \"no-cache\",\n" +
            "    \"pragma\": \"no-cache\",\n" +
            "    \"priority\": \"u=0, i\",\n" +
            "    \"sec-ch-ua\": \"\\\"Not)A;Brand\\\";v=\\\"99\\\", \\\"Microsoft Edge\\\";v=\\\"127\\\", \\\"Chromium\\\";v=\\\"127\\\"\",\n" +
            "    \"sec-ch-ua-mobile\": \"?0\",\n" +
            "    \"sec-ch-ua-platform\": \"\\\"Windows\\\"\",\n" +
            "    \"sec-fetch-dest\": \"document\",\n" +
            "    \"sec-fetch-mode\": \"navigate\",\n" +
            "    \"sec-fetch-site\": \"none\",\n" +
            "    \"sec-fetch-user\": \"?1\",\n" +
            "    \"upgrade-insecure-requests\": \"1\",\n" +
            "    \"cookie\": \"_tea_utm_cache_2608=undefined; __tea_cookie_tokens_2608=%257B%2522web_id%2522%253A%25227392842114574763557%2522%252C%2522user_unique_id%2522%253A%25227392842114574763557%2522%252C%2522timestamp%2522%253A1721280204990%257D; passport_csrf_token=feef8f93ddc76e2a19979de26d301369; passport_csrf_token_default=feef8f93ddc76e2a19979de26d301369; n_mh=pWOT_m9BARSnfZ7Mv0VPJej8h6mQKqMejyiLQYKZmM8; passport_auth_status=7fc2132f1f42d6a6f873b30ba7c873b0%2C; passport_auth_status_ss=7fc2132f1f42d6a6f873b30ba7c873b0%2C; sid_guard=05a7571bf1ec5e0b8c108d929b60f4ee%7C1722823936%7C31536000%7CTue%2C+05-Aug-2025+02%3A12%3A16+GMT; uid_tt=0a238b3c54dbf27766dd1e1487822b29; uid_tt_ss=0a238b3c54dbf27766dd1e1487822b29; sid_tt=05a7571bf1ec5e0b8c108d929b60f4ee; sessionid=05a7571bf1ec5e0b8c108d929b60f4ee; sessionid_ss=05a7571bf1ec5e0b8c108d929b60f4ee; is_staff_user=false; sid_ucp_v1=1.0.0-KGUzZTlhZjNhNDYyYTI4YmU4MzMyOGNmN2U3M2Q5Mzc4ODc3YTUxOTkKFwjHkbDA_fWZAxCA6sC1BhiwFDgCQPEHGgJsZiIgMDVhNzU3MWJmMWVjNWUwYjhjMTA4ZDkyOWI2MGY0ZWU; ssid_ucp_v1=1.0.0-KGUzZTlhZjNhNDYyYTI4YmU4MzMyOGNmN2U3M2Q5Mzc4ODc3YTUxOTkKFwjHkbDA_fWZAxCA6sC1BhiwFDgCQPEHGgJsZiIgMDVhNzU3MWJmMWVjNWUwYjhjMTA4ZDkyOWI2MGY0ZWU; store-region=cn-jl; store-region-src=uid; _tea_utm_cache_576092=undefined; csrf_session_id=82bb2def4b3399042cb195180152420b\"\n" +
            "  }";
    public static final String jsons = "[{\"id\":1,\"name\":\"张1\"},{\"id\":2,\"name\":\"张2\"},{\"id\":3,\"name\":\"张3\"},{\"id\":4,\"name\":\"张4\"}]";
    public final JsonToolBean bean = JsonToolBean.builder().id(1).name("张1").build();
    public static final List<JsonToolBean> beans = Arrays.asList(
            JsonToolBean.builder().id(1).name("张1").build(),
            JsonToolBean.builder().id(2).name("张2").build(),
            JsonToolBean.builder().id(3).name("张3").build(),
            JsonToolBean.builder().id(4).name("张4").build()
    );

    /**
     *
     */
    public static final String esJson = "{\n" +
            "\t\"took\": 1,\n" +
            "\t\"timed_out\": false,\n" +
            "\t\"_shards\": {\n" +
            "\t\t\"total\": 1,\n" +
            "\t\t\"successful\": 1,\n" +
            "\t\t\"skipped\": 0,\n" +
            "\t\t\"failed\": 0\n" +
            "\t},\n" +
            "\t\"hits\": {\n" +
            "\t\t\"total\": 6,\n" +
            "\t\t\"max_score\": 13.779228,\n" +
            "\t\t\"hits\": [\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"_index\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_type\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_id\": \"bc457a2b99a144b48b09804c86413bb9\",\n" +
            "\t\t\t\t\"_score\": 13.779228,\n" +
            "\t\t\t\t\"_source\": {\n" +
            "\t\t\t\t\t\"electricity\": 68,\n" +
            "\t\t\t\t\t\"endTime\": 1635966411000,\n" +
            "\t\t\t\t\t\"formatEndTime\": \"\",\n" +
            "\t\t\t\t\t\"formatStartTime\": \"\",\n" +
            "\t\t\t\t\t\"formatSyncTime\": \"\",\n" +
            "\t\t\t\t\t\"speed\": false,\n" +
            "\t\t\t\t\t\"startTime\": 1635921641000,\n" +
            "\t\t\t\t\t\"vin\": \"LNBMCB4K4MZ012219\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"_index\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_type\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_id\": \"f165f40c92974edeb0dd5b20156602bd\",\n" +
            "\t\t\t\t\"_score\": 13.779228,\n" +
            "\t\t\t\t\"_source\": {\n" +
            "\t\t\t\t\t\"electricity\": 3.97,\n" +
            "\t\t\t\t\t\"endTime\": 1637620425000,\n" +
            "\t\t\t\t\t\"formatEndTime\": \"\",\n" +
            "\t\t\t\t\t\"formatStartTime\": \"\",\n" +
            "\t\t\t\t\t\"formatSyncTime\": \"\",\n" +
            "\t\t\t\t\t\"speed\": false,\n" +
            "\t\t\t\t\t\"startTime\": 1637616728000,\n" +
            "\t\t\t\t\t\"vin\": \"LNBMCB4K4MZ012219\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"_index\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_type\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_id\": \"3e1c21c189b8494e92e48ef0a66c5a7b\",\n" +
            "\t\t\t\t\"_score\": 13.779228,\n" +
            "\t\t\t\t\"_source\": {\n" +
            "\t\t\t\t\t\"electricity\": 62.66,\n" +
            "\t\t\t\t\t\"endTime\": 1638214643000,\n" +
            "\t\t\t\t\t\"formatEndTime\": \"\",\n" +
            "\t\t\t\t\t\"formatStartTime\": \"\",\n" +
            "\t\t\t\t\t\"formatSyncTime\": \"\",\n" +
            "\t\t\t\t\t\"speed\": false,\n" +
            "\t\t\t\t\t\"startTime\": 1638174487000,\n" +
            "\t\t\t\t\t\"vin\": \"LNBMCB4K4MZ012219\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"_index\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_type\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_id\": \"2910e18504044a20afc1b6f4e0f4fff0\",\n" +
            "\t\t\t\t\"_score\": 13.779228,\n" +
            "\t\t\t\t\"_source\": {\n" +
            "\t\t\t\t\t\"electricity\": 9.18,\n" +
            "\t\t\t\t\t\"endTime\": 1638221701000,\n" +
            "\t\t\t\t\t\"formatEndTime\": \"\",\n" +
            "\t\t\t\t\t\"formatStartTime\": \"\",\n" +
            "\t\t\t\t\t\"formatSyncTime\": \"\",\n" +
            "\t\t\t\t\t\"speed\": false,\n" +
            "\t\t\t\t\t\"startTime\": 1638214645000,\n" +
            "\t\t\t\t\t\"vin\": \"LNBMCB4K4MZ012219\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"_index\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_type\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_id\": \"74e35367b4fb44a7ae51385259874fe4\",\n" +
            "\t\t\t\t\"_score\": 13.779228,\n" +
            "\t\t\t\t\"_source\": {\n" +
            "\t\t\t\t\t\"electricity\": 73.43,\n" +
            "\t\t\t\t\t\"endTime\": 1636673592000,\n" +
            "\t\t\t\t\t\"formatEndTime\": \"\",\n" +
            "\t\t\t\t\t\"formatStartTime\": \"\",\n" +
            "\t\t\t\t\t\"formatSyncTime\": \"\",\n" +
            "\t\t\t\t\t\"speed\": false,\n" +
            "\t\t\t\t\t\"startTime\": 1636625322000,\n" +
            "\t\t\t\t\t\"vin\": \"LNBMCB4K4MZ012219\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"_index\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_type\": \"intest_electricity\",\n" +
            "\t\t\t\t\"_id\": \"2839a14550f54c38bcdaf26f7956e3f7\",\n" +
            "\t\t\t\t\"_score\": 13.779228,\n" +
            "\t\t\t\t\"_source\": {\n" +
            "\t\t\t\t\t\"electricity\": 58.31,\n" +
            "\t\t\t\t\t\"endTime\": 1637616726000,\n" +
            "\t\t\t\t\t\"formatEndTime\": \"\",\n" +
            "\t\t\t\t\t\"formatStartTime\": \"\",\n" +
            "\t\t\t\t\t\"formatSyncTime\": \"\",\n" +
            "\t\t\t\t\t\"speed\": false,\n" +
            "\t\t\t\t\t\"startTime\": 1637579066000,\n" +
            "\t\t\t\t\t\"vin\": \"LNBMCB4K4MZ012219\"\n" +
            "\t\t\t\t}\n" +
            "\t\t\t}\n" +
            "\t\t]\n" +
            "\t}\n" +
            "}";

    @Test
    public void getJsonToBean() {
        JsonToolBean jsonToBean = JsonTool.getJsonToBean(json, JsonToolBean.class);
        System.out.println(jsonToBean);
    }

    @Test
    public void getJsonToBeans() {
        List<JsonToolBean> jsonToBeans = JsonTool.getJsonToBeans(jsons, JsonToolBean.class);
        System.out.println(jsonToBeans);
    }

    @Test
    public void getJsonToMap() {
        Map<String, Object> jsonToMap = JsonTool.getJsonToMap(json);
        System.out.println(jsonToMap);


    }

    @Test
    public void getJsonToMaps() {
        List<Map<String, Object>> jsonToMaps = JsonTool.getJsonToMaps(jsons);
        System.out.println(jsonToMaps);
    }

    @Test
    public void getJsonToStringMap() {
        Map<String, String> jsonToStringMap = JsonTool.getJsonToStringMap(json);
        System.out.println(jsonToStringMap);
    }

    @Test
    public void getObjToBean() {
        JsonToolBean objToBean = JsonTool.getObjToBean(bean, JsonToolBean.class);
        System.out.println(objToBean);
    }

    @Test
    public void getObjToBeans() {
        List<JsonToolBean> objToBeans = JsonTool.getObjToBeans(beans, JsonToolBean.class);
        System.out.println(objToBeans);
    }

    @Test
    public void getObjToMap() {
        Map<String, Object> objToMap = JsonTool.getObjToMap(bean);
        System.out.println(objToMap);
    }

    @Test
    public void getObjToMaps() {
        List<Map<String, Object>> objToMaps = JsonTool.getObjToMaps(beans);
        System.out.println(objToMaps);
    }

    @Test
    public void getObjToStringMap() {
        Map<String, String> objToStringMap = JsonTool.getObjToStringMap(bean);
        System.out.println(objToStringMap);
    }

    @Test
    public void getObjToJson() {
        String objToJson = JsonTool.getObjToJson(beans);
        System.out.println(objToJson);

    }

    @Test
    public void valid() {
        JsonTool.valid(json);
    }

    @Test
    void testGetJsonToBean() {
    }
}

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
class JsonToolBean {
    private int id;
    private String name;
}